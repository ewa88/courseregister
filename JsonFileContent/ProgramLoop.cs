﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonFileContent.Interface;
using JsonFileContent.IoHelpers;

namespace JsonFileContent
{
   internal class ProgramLoop
   {
       private IJsonMapper _jsonMapper;
       private ICalculateFileDetails _calculateFileDetails;

       public ProgramLoop()
       {
           _jsonMapper = new JsonMapper();
           _calculateFileDetails = new CalculateFileDetails();
       }
        public void Execute()
        {
            bool loop = true;
            while(loop)
            try
            {
                string filepath = ConsoleReadHelper.GetString("Wpisz scieżkę do pliku: ");
                if (!filepath.EndsWith(".json"))
                {
                    filepath = filepath + ".json";
                }
                    //  const string filepath = @"C:\Users\Student13\Desktop\Homework\Homework_ETAP2_TK_2\homework_2\test.json";
                    var fileData = _jsonMapper.ReadJsonFile(filepath);
                _calculateFileDetails.CalculateDetailsFromFile(fileData);
                loop = false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Podana ścieżka nie istnieje. ");               
            }       
        }
    }
}
