﻿using System;
using System.Collections.Generic;

namespace JsonFileContent
{
   internal class FileData
   {  
        public string CourseName { get; set; }
        public string TrainerName { get; set; }
        public int PresenceThreshold { get; set; }
        public int HomeworkThreshold { get; set; }

        public List<StudentRaport> StudentsList  { get; set; }
    }
       public class StudentRaport
       {
            public int PresenceResult { get; set; }
            public float HomeworkResult { get; set; }
       }
    
    }
