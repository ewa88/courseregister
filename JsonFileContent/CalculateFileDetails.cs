﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using JsonFileContent.Interface;
using JsonFileContent.IoHelpers;

namespace JsonFileContent
{
    internal class CalculateFileDetails : ICalculateFileDetails
    {
        public void CalculateDetailsFromFile(FileData fileData)
        {
            float result = 0;
            int presenceReuslt = 0;
            int studentsPassedHomework = 0;
            int studentsPassedPresence = 0;
            float averageHomewrokResult;
            int averagePresenceResult;
            foreach (var student in fileData.StudentsList)
            {
                result = result + student.HomeworkResult;
                presenceReuslt = presenceReuslt + student.PresenceResult;
                if (student.HomeworkResult >= fileData.HomeworkThreshold)
                {
                    studentsPassedHomework++;
                }
                if (student.PresenceResult >= fileData.PresenceThreshold)
                {
                    studentsPassedPresence++;
                }

            }
            averageHomewrokResult = result / fileData.StudentsList.Count;
            averagePresenceResult = presenceReuslt / fileData.StudentsList.Count;
            ConsoleWriteHelper.PrintFileDetails(fileData, averagePresenceResult, averageHomewrokResult, studentsPassedHomework, studentsPassedPresence );
        }
    }
}