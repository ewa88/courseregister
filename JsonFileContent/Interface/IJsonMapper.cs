namespace JsonFileContent.Interface
{
    internal interface IJsonMapper
    {
        FileData ReadJsonFile(string filepath);
    }
}