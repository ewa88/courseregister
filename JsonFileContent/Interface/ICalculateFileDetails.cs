namespace JsonFileContent.Interface
{
    internal interface ICalculateFileDetails
    {
        void CalculateDetailsFromFile(FileData fileData);
    }
}