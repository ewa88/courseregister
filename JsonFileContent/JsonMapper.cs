﻿using System;
using System.Collections.Generic;
using System.IO;
using JsonFileContent.Interface;
using Newtonsoft.Json;

namespace JsonFileContent
{
    internal class JsonMapper : IJsonMapper
    {
        public FileData ReadJsonFile(string filepath)
        {
            var fileData = new FileData();
            using (var file = File.OpenText(filepath))
            {
                fileData = (FileData) new JsonSerializer().Deserialize(file, typeof(FileData));
            }
            return fileData;

        }
    }
}
