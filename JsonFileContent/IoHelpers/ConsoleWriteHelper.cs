﻿using System;

namespace JsonFileContent.IoHelpers
{
    internal class ConsoleWriteHelper
    {
        public static void PrintFileDetails(FileData fileData, int averagePresenceResult, float averageHomewrokResult, int studentsPassedHomework, int studentsPassedPresence)
        {
            Console.WriteLine("\nKurs: " + fileData.CourseName);
            Console.WriteLine("Trener: " + fileData.TrainerName);
            Console.WriteLine("Srednia z prac domowych: " + averageHomewrokResult + "%");
            Console.WriteLine("Srednia z obecnosci: " + averagePresenceResult+ "%");
            Console.WriteLine("Prace domowa zaliczyly: " + studentsPassedHomework+ " osoby");
            Console.WriteLine("Obecnosc zaliczyly: " + studentsPassedPresence + " osoby\n");
        }
    }
}
