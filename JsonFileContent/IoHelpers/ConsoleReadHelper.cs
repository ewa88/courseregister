﻿using System;

namespace JsonFileContent.IoHelpers
{
    internal class ConsoleReadHelper
    {
        public static string GetString(string message)
        {
            string answer = "";
            Console.WriteLine(message);
            answer = Console.ReadLine();
            return answer;
        }
    }
}
