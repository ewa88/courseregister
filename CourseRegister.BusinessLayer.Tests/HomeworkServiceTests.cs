﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Service;
using CourseRegister.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class HomeworkServiceTests
    {
        [TestMethod]
        public void SumMaxHomeworkResult_CourseDtoInput_SumMaxHomeworkResultOutput()
        {
          
            var testCourse = new CourseDto();

            //lista zadan
            testCourse.HomeworkList = new List<HomeworkDto>();

            HomeworkDto testHomeworkDto = new HomeworkDto();
            testHomeworkDto.Id = 1;
            testHomeworkDto.MaxResult = 100;
            testCourse.HomeworkList.Add(testHomeworkDto);

            HomeworkDto testHomeworkDto1 = new HomeworkDto();
            testHomeworkDto1.Id = 1;
            testHomeworkDto1.MaxResult = 50;
            testCourse.HomeworkList.Add(testHomeworkDto1);

            HomeworkDto testHomeworkDto3 = new HomeworkDto();
            testHomeworkDto3.Id = 1;
            testHomeworkDto3.MaxResult = 200;
            testCourse.HomeworkList.Add(testHomeworkDto3);

            var homeworkService = new HomeworkService();

            var result = homeworkService.MaxResult(testCourse);

            Assert.AreEqual(350, result);

        }

        [TestMethod]
        public void SumHomeworkResult_CourseDtoCourseStudentInput_SumResultOutput()
        {
            var testCourse = new CourseDto();
            testCourse.CourseStudentList = new List<CourseStudentDto>();

            //pierwszy kurstant
            CourseStudentDto testStudent1 = new CourseStudentDto();
            testStudent1.Id = 2;
            testStudent1.Pesel = 11111111111;
            testCourse.CourseStudentList.Add(testStudent1);

            //drugi kursant
            CourseStudentDto testStudent2 = new CourseStudentDto();
            testStudent2.Id = 4;
            testStudent2.Pesel = 22222222222;
            testCourse.CourseStudentList.Add(testStudent2);
            //lista zadan
            testCourse.HomeworkList = new List<HomeworkDto>();
            HomeworkDto testHomeworkDto = new HomeworkDto();
            testHomeworkDto.Id = 1;
            testHomeworkDto.MaxResult = 100;
            testCourse.HomeworkList.Add(testHomeworkDto);

            testHomeworkDto.HomeworkResultList = new List<HomeworkResultDto>();
            HomeworkResultDto homeworkResultDto1 = new HomeworkResultDto();

            homeworkResultDto1.Id = 1;
            homeworkResultDto1.Pesel = 11111111111; //pesel pierwszego kursanta
            homeworkResultDto1.Result = 80;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto1);

            HomeworkResultDto homeworkResultDto2 = new HomeworkResultDto();

            homeworkResultDto2.Id = 2;
            homeworkResultDto2.Pesel = 22222222222; //pesel drugiego kursanta
            homeworkResultDto2.Result = 90;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto2);

            HomeworkResultDto homeworkResultDto3 = new HomeworkResultDto();

            homeworkResultDto3.Id = 1;
            homeworkResultDto3.Pesel = 11111111111; //pesel pierwszego kursanta
            homeworkResultDto3.Result = 120;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto3);

            HomeworkResultDto homeworkResultDto4 = new HomeworkResultDto();

            homeworkResultDto4.Id = 2;
            homeworkResultDto4.Pesel = 22222222222; //pesel drugiego kursanta
            homeworkResultDto4.Result = 210;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto4);

            var homeworkService = new HomeworkService();

            var result = homeworkService.SumResult(testCourse, testStudent1);
            var result1 = homeworkService.SumResult(testCourse, testStudent2);

            Assert.AreEqual(200, result);
            Assert.AreEqual(300, result1);

        }
    }
}
