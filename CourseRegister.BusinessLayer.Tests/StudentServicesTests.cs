﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.BusinessLayer.Service;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class StudentServicesTests
    {
        [TestMethod]
        public void GetStudentByPesel_StudentPeselInput_StudentDtoOutput()
        {
              var testStudent = new Student();
            testStudent.PESEL = 88052911640;
            List<Student> studentsList = new List<Student>();
            studentsList.Add(testStudent);

            var expectedStudent=new StudentDto();
            expectedStudent.PESEL = 88052911640;
            List<StudentDto> studentDtoList = new List<StudentDto>();
            studentDtoList.Add(expectedStudent);

            var studentRepositoryMock = new Mock<IStudentRepository>();
            studentRepositoryMock.Setup(x => x.GetStudentByPesel(88052911640)).Returns(studentsList);

            var studentServices = new StudentService(studentRepositoryMock.Object);

            var result = studentServices.GetStudentByPesel(88052911640);

            Assert.AreEqual(expectedStudent.PESEL,result.PESEL);
            studentRepositoryMock.Verify(x => x.GetStudentByPesel(testStudent.PESEL), Times.Once);
        }
    }
}
