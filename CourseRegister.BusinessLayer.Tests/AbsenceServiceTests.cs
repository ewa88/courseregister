﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Service;
using CourseRegister.DataLayer.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class AbsenceServiceTests
    {
        [TestMethod]
        public void GetPresenceDaysNumber_CourseStudentDtoInput_PresenceDaysOutput()
        {
         
            var courseStudentsDto = new CourseStudentDto();
            courseStudentsDto.Id = 5;
            courseStudentsDto.Pesel = 88052911640;

            AbsenceDto absence = new AbsenceDto();
            absence.Id = 1;
            absence.Date = DateTime.Today;
            absence.Presence= AbsenceDto.TypeAbsence.o;

            AbsenceDto absence1 = new AbsenceDto();
            absence1.Id = 2;
            absence1.Date = DateTime.Parse("2017/06/07");
            absence1.Presence = AbsenceDto.TypeAbsence.n;

            AbsenceDto absence2 = new AbsenceDto();
            absence2.Id = 2;
            absence2.Date = DateTime.Parse("2017/06/08");
            absence2.Presence = AbsenceDto.TypeAbsence.o;

            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence);
            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence1);
            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence2);

            var absenceService=new AbsenceService();
            var result = absenceService.GetPresenceDaysNumber(courseStudentsDto);
                     
            Assert.AreEqual(2,result);
        }

        [TestMethod]
        public void GetDaysNumber_CourseStudentDtoInput_DaysNumberOutput()
        {
            var courseStudentsDto = new CourseStudentDto();
            courseStudentsDto.Id = 5;
            courseStudentsDto.Pesel = 88052911640;

            AbsenceDto absence = new AbsenceDto();
            absence.Id = 1;
            absence.Date = DateTime.Today;
            absence.Presence = AbsenceDto.TypeAbsence.o;

            AbsenceDto absence1 = new AbsenceDto();
            absence1.Id = 2;
            absence1.Date = DateTime.Parse("2017/06/07");
            absence1.Presence = AbsenceDto.TypeAbsence.n;

            AbsenceDto absence2 = new AbsenceDto();
            absence2.Id = 2;
            absence2.Date = DateTime.Parse("2017/06/08");
            absence2.Presence = AbsenceDto.TypeAbsence.o;

            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence);
            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence1);
            courseStudentsDto.AbsenceListEachCourseStudent.Add(absence2);

            var absenceService = new AbsenceService();
            var result = absenceService.GetDaysNumber(courseStudentsDto);

            Assert.AreEqual(3, result);

        }
    }
}
