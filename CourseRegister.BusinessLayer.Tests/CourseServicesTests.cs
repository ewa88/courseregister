﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Service;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class CourseServicesTests
    {
        [TestMethod]
        public void CheckIfCourseExist_CourseIdInput_boolOutput()
        {
            var course = new Course();
            course.CourseId = 5;
            List< Course> courseList = new List<Course>();
            courseList.Add(course);

            var courseRepositoryMock = new Mock<ICourseRepository>();
            courseRepositoryMock.Setup(x => x.GetCourseById(5)).Returns(courseList);

            var courseServices = new CourseServices(courseRepositoryMock.Object);

            var result = courseServices.CheckIfCourseExist(5);
            var resultFail = courseServices.CheckIfCourseExist(4);

            Assert.AreEqual(true, result);
            Assert.AreEqual(false, resultFail);

            courseRepositoryMock.Verify(x => x.GetCourseById(course.CourseId),Times.Once);
        }

        [TestMethod]
        public void GetCourseById_CourseIdInput_CourseDtoOutput()
        {
            var testCourse = new Course();
            testCourse.CourseId = 2;
            testCourse.Id = 4;
            testCourse.Name = "Name";
            testCourse.CourseStudentList = new List<CourseStudent>();
            testCourse.HomeworkList = new List<Homework>();

            List<Course> testCourseList = new List<Course>();
            testCourseList.Add(testCourse);

            var expectedCourse = new CourseDto();
            expectedCourse.CourseId = 2;
            expectedCourse.Id = 4;
            expectedCourse.Name = "Name";
            expectedCourse.CourseStudentList = new List<CourseStudentDto>();
            expectedCourse.HomeworkList = new List<HomeworkDto>();

            List<CourseDto> expectedCourseList = new List<CourseDto>();
            expectedCourseList.Add(expectedCourse);

            var courseRepositoryMock = new Mock<ICourseRepository>();
            courseRepositoryMock.Setup(x => x.GetCourseById(2)).Returns(testCourseList);

            var courseServices = new CourseServices(courseRepositoryMock.Object);

            var result = courseServices.GetCourseById(2);

            Assert.AreEqual(expectedCourseList.Count, result.Count);
            Assert.AreEqual(expectedCourseList[0].CourseId, result[0].CourseId);
            Assert.AreEqual(expectedCourseList[0].Id, result[0].Id);
            Assert.AreEqual(expectedCourseList[0].Name, result[0].Name);

            courseRepositoryMock.Verify(x => x.GetCourseById(testCourse.CourseId), Times.Once);

        }
    }
}
