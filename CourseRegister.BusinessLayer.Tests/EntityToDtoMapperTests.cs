﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class EntityToDtoMapperTests
    {
        [TestMethod]
        public void CheckStudentMapper_TestStudentEntityInput_MappedStudentDtoOutput()
        {
            Student testStudent = new Student();
            testStudent.Id = 1;
            testStudent.BirthDate= DateTime.Today;
            testStudent.Name = "Adam";
            testStudent.Surname = "Kowalski";
            testStudent.PESEL = 88052911640;
            testStudent.Sex = Student.TypeSex.M;

            StudentDto expectedStudent = new StudentDto();
            expectedStudent.Id = 1;
            expectedStudent.BirthDate = DateTime.Today;
            expectedStudent.Name = "Adam";
            expectedStudent.Surname = "Kowalski";
            expectedStudent.PESEL = 88052911640;
            expectedStudent.Sex = StudentDto.TypeSex.M;

            StudentDto resultStudent = EntityToDtoMapper.StudentEntityToDtoModel(testStudent);

            //Assert.AreEqual(expectedStudent, resultStudent); dlaczego nie przechodzi testu?

            Assert.AreEqual(expectedStudent.Id, resultStudent.Id);
            Assert.AreEqual(expectedStudent.BirthDate,resultStudent.BirthDate);
            Assert.AreEqual(expectedStudent.Name, resultStudent.Name);
            Assert.AreEqual(expectedStudent.Surname, resultStudent.Surname);
            Assert.AreEqual(expectedStudent.PESEL, resultStudent.PESEL);
            Assert.AreEqual(expectedStudent.Sex, resultStudent.Sex);
            
        }

        [TestMethod]
        public void CheckCourseMapper_CourseEntityInput_MappedTestCourseDtoOutput()
        {
            Course testCourse = new Course();
            testCourse.Id = 1;
            testCourse.Name = "Junior C# Devepoler";
            testCourse.CourseId = 10;
            testCourse.Date = DateTime.Today;
            testCourse.HomeworkThreshold = 80;
            testCourse.PresenceThreshold = 70;
            testCourse.NumberOfStudents = 12;
            testCourse.TrainerName = "Jakub Bulczak";

            testCourse.CourseStudentList = new List<CourseStudent>();

            //pierwszy kurstant
            CourseStudent testStudent1 = new CourseStudent();
            testStudent1.Id = 2;
            testStudent1.Pesel = 11111111111;
            testCourse.CourseStudentList.Add(testStudent1);

            //drugi kursant
            CourseStudent testStudent2 = new CourseStudent();
            testStudent2.Id = 4;
            testStudent2.Pesel = 22222222222;
            testCourse.CourseStudentList.Add(testStudent2);

            //lista obecnosci pierwszego kursanta
            testStudent1.AbsenceListEachCourseStudent = new List<Absence>();
            Absence testAbsence1 = new Absence();
            testAbsence1.Id = 1;
            testAbsence1.Date = DateTime.Today;
            testAbsence1.Presence = Absence.TypeAbsence.n;
            testStudent1.AbsenceListEachCourseStudent.Add(testAbsence1);

            //lista obecnosci drugiego kursanta
            testStudent2.AbsenceListEachCourseStudent = new List<Absence>();
            Absence testAbsence2 = new Absence();
            testAbsence2.Id = 2;
            testAbsence2.Date = DateTime.Today;
            testAbsence2.Presence = Absence.TypeAbsence.o;
            testStudent2.AbsenceListEachCourseStudent.Add(testAbsence2);

            //lista zadan
            testCourse.HomeworkList = new List<Homework>();
            Homework testHomework = new Homework();
            testHomework.Id = 1;
            testHomework.MaxResult = 100;
            testCourse.HomeworkList.Add(testHomework);

            testHomework.HomeworkResultList = new List<HomeworkResult>();
            HomeworkResult homeworkResult1 = new HomeworkResult();

            homeworkResult1.Id = 1;
            homeworkResult1.Pesel = 11111111111; //pesel pierwszego kursanta
            homeworkResult1.Result = 80;
            testHomework.HomeworkResultList.Add(homeworkResult1);

            HomeworkResult homeworkResult2 = new HomeworkResult();

            homeworkResult2.Id = 2;
            homeworkResult2.Pesel = 22222222222; //pesel drugiego kursanta
            homeworkResult2.Result = 90;
            testHomework.HomeworkResultList.Add(homeworkResult2);

            //koniec test course

            //poczatek expected course

            CourseDto expectedCourse = new CourseDto();
            expectedCourse.Id = 1;
            expectedCourse.Name = "Junior C# Devepoler";
            expectedCourse.CourseId = 10;
            expectedCourse.Date = DateTime.Today;
            expectedCourse.HomeworkThreshold = 80;
            expectedCourse.PresenceThreshold = 70;
            expectedCourse.NumberOfStudents = 12;
            expectedCourse.TrainerName = "Jakub Bulczak";

            expectedCourse.CourseStudentList = new List<CourseStudentDto>();

            //pierwszy kursant
            CourseStudentDto expectedCourseStudent1 = new CourseStudentDto();
            expectedCourseStudent1.Id = 2;
            expectedCourseStudent1.Pesel = 11111111111;
            expectedCourse.CourseStudentList.Add(expectedCourseStudent1);

            //drugi kurstant
            CourseStudentDto expectedCourseStudent2 = new CourseStudentDto();
            expectedCourseStudent2.Id = 4;
            expectedCourseStudent2.Pesel = 22222222222;
            expectedCourse.CourseStudentList.Add(expectedCourseStudent2);

            //lista obecnosci pierwszego kursanta
            expectedCourseStudent1.AbsenceListEachCourseStudent = new List<AbsenceDto>();
            AbsenceDto expectedAbsence1 = new AbsenceDto();
            expectedAbsence1.Id = 1;
            expectedAbsence1.Date = DateTime.Today;
            expectedAbsence1.Presence = AbsenceDto.TypeAbsence.n;
            expectedCourseStudent1.AbsenceListEachCourseStudent.Add(expectedAbsence1);

            //lista obecnosci drugiego kursanta
            expectedCourseStudent2.AbsenceListEachCourseStudent = new List<AbsenceDto>();
            AbsenceDto expectedAbsence2 = new AbsenceDto();
            expectedAbsence2.Id = 2;
            expectedAbsence2.Date = DateTime.Today;
            expectedAbsence2.Presence = AbsenceDto.TypeAbsence.o;
            expectedCourseStudent2.AbsenceListEachCourseStudent.Add(expectedAbsence2);

            //lista zadan
            expectedCourse.HomeworkList = new List<HomeworkDto>();
            HomeworkDto expectedHomework = new HomeworkDto();
            expectedHomework.Id = 1;
            expectedHomework.MaxResult = 100;

            expectedCourse.HomeworkList.Add(expectedHomework);
            expectedHomework.HomeworkResultList = new List<HomeworkResultDto>();

            HomeworkResultDto expectedHomeworkResult1 = new HomeworkResultDto();
            expectedHomeworkResult1.Id = 1;
            expectedHomeworkResult1.Pesel = 11111111111; //pesel pierwszego kursanta
            expectedHomeworkResult1.Result = 80;
            expectedHomework.HomeworkResultList.Add(expectedHomeworkResult1);

            HomeworkResultDto expectedHomeworkResult2 = new HomeworkResultDto();
            expectedHomeworkResult2.Id = 2;
            expectedHomeworkResult2.Pesel = 22222222222; //pesel drugiego kursanta
            expectedHomeworkResult2.Result = 90;
            expectedHomework.HomeworkResultList.Add(expectedHomeworkResult2);

            //mapowanie obiektu testCourse
            CourseDto resultCourse = EntityToDtoMapper.CourseEntityToDto(testCourse);

            Assert.AreEqual(expectedCourse.Id, resultCourse.Id);
            Assert.AreEqual(expectedCourse.CourseId, resultCourse.CourseId);
            Assert.AreEqual(expectedCourse.Date, resultCourse.Date);
            Assert.AreEqual(expectedCourse.HomeworkThreshold, resultCourse.HomeworkThreshold);
            Assert.AreEqual(expectedCourse.PresenceThreshold, resultCourse.PresenceThreshold);
            Assert.AreEqual(expectedCourse.NumberOfStudents, resultCourse.NumberOfStudents);
            Assert.AreEqual(expectedCourse.TrainerName, resultCourse.TrainerName);
            Assert.AreEqual(expectedCourse.Name, resultCourse.Name);

            //test kursanta nr 1
            Assert.AreEqual(expectedCourse.CourseStudentList[0].Id, resultCourse.CourseStudentList[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].Pesel, resultCourse.CourseStudentList[0].Pesel);

            //lista obecnosci kursanta nr 1
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Id, resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Date, resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Date);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Presence, resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Presence);

            //ogolna lista zadan
            Assert.AreEqual(expectedCourse.HomeworkList[0].Id, resultCourse.HomeworkList[0].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].MaxResult, resultCourse.HomeworkList[0].MaxResult);

            //lista wynikow zadan kursanta nt 1
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Id, resultCourse.HomeworkList[0].HomeworkResultList[0].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Pesel, resultCourse.HomeworkList[0].HomeworkResultList[0].Pesel);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Result, resultCourse.HomeworkList[0].HomeworkResultList[0].Result);

            // test kursanta nr 2
            Assert.AreEqual(expectedCourse.CourseStudentList[1].Id, resultCourse.CourseStudentList[1].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].Pesel, resultCourse.CourseStudentList[1].Pesel);

            //lista obecnosci kursanta nr 2 
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Id, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Date, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Date);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Presence, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Presence);

            //lista wynikow zadan kursanta nt 2
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Id, resultCourse.HomeworkList[0].HomeworkResultList[1].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Pesel, resultCourse.HomeworkList[0].HomeworkResultList[1].Pesel);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Result, resultCourse.HomeworkList[0].HomeworkResultList[1].Result);













        }
    }
}
