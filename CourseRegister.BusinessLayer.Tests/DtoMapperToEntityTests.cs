﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.BusinessLayer.Tests
{
    [TestClass]
    public class DtoMapperToEntityTests
    {
        [TestMethod]
        public void CheckStudentMapper_StudentDtoInput_MappedTestStudentEntityOutput()
        {
            StudentDto testStudent = new StudentDto();
            testStudent.Id = 1;
            testStudent.BirthDate = DateTime.Today;
            testStudent.Name = "Adam";
            testStudent.Surname = "Kowalski";
            testStudent.PESEL = 88052911640;
            testStudent.Sex = StudentDto.TypeSex.M;

            Student expectedStudent = new Student();
            expectedStudent.Id = 1;
            expectedStudent.BirthDate = DateTime.Today;
            expectedStudent.Name = "Adam";
            expectedStudent.Surname = "Kowalski";
            expectedStudent.PESEL = 88052911640;
            expectedStudent.Sex = Student.TypeSex.M;

            Student resultStudent = DtoToEntityMapper.StudentDtoToEntityModel(testStudent);

            Assert.AreEqual(expectedStudent.Id, resultStudent.Id);
            Assert.AreEqual(expectedStudent.BirthDate, resultStudent.BirthDate);
            Assert.AreEqual(expectedStudent.Name, resultStudent.Name);
            Assert.AreEqual(expectedStudent.Surname,resultStudent.Surname);
            Assert.AreEqual(expectedStudent.PESEL,resultStudent.PESEL);
            Assert.AreEqual(expectedStudent.Sex, resultStudent.Sex);

        }

        [TestMethod]
        public void CheckCourseMapper_CourseDtoInput_MappedTestCourseEntityOutput()
        {
            CourseDto testCourse = new CourseDto();
            testCourse.Id = 1;
            testCourse.Name = "Junior C# Devepoler";
            testCourse.CourseId = 10;
            testCourse.Date = DateTime.Today;
            testCourse.HomeworkThreshold = 80;
            testCourse.PresenceThreshold = 70;
            testCourse.NumberOfStudents = 12;
            testCourse.TrainerName = "Jakub Bulczak";
            
            testCourse.CourseStudentList = new List<CourseStudentDto>();
            
            //pierwszy kurstant
            CourseStudentDto testStudent1 = new CourseStudentDto();
            testStudent1.Id = 2;
            testStudent1.Pesel = 11111111111;
            testCourse.CourseStudentList.Add(testStudent1);

            //drugi kursant
            CourseStudentDto testStudent2 = new CourseStudentDto();
            testStudent2.Id = 4;
            testStudent2.Pesel = 22222222222;
            testCourse.CourseStudentList.Add(testStudent2);

            //lista obecnosci pierwszego kursanta
            testStudent1.AbsenceListEachCourseStudent = new List<AbsenceDto>();
            AbsenceDto testAbsence1 = new AbsenceDto();
            testAbsence1.Id = 1;
            testAbsence1.Date = DateTime.Today;
            testAbsence1.Presence = AbsenceDto.TypeAbsence.n;
            testStudent1.AbsenceListEachCourseStudent.Add(testAbsence1);

            //lista obecnosci drugiego kursanta
            testStudent2.AbsenceListEachCourseStudent= new List<AbsenceDto>();
            AbsenceDto testAbsence2 = new AbsenceDto();
            testAbsence2.Id = 2;
            testAbsence2.Date = DateTime.Today;
            testAbsence2.Presence = AbsenceDto.TypeAbsence.o;
            testStudent2.AbsenceListEachCourseStudent.Add(testAbsence2);

            //lista zadan
            testCourse.HomeworkList = new List<HomeworkDto>();
            HomeworkDto testHomeworkDto = new HomeworkDto();
            testHomeworkDto.Id = 1;
            testHomeworkDto.MaxResult = 100;
            testCourse.HomeworkList.Add(testHomeworkDto);

            testHomeworkDto.HomeworkResultList = new List<HomeworkResultDto>();
            HomeworkResultDto homeworkResultDto1 = new HomeworkResultDto();

            homeworkResultDto1.Id = 1;
            homeworkResultDto1.Pesel = 11111111111; //pesel pierwszego kursanta
            homeworkResultDto1.Result = 80;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto1);

            HomeworkResultDto homeworkResultDto2 = new HomeworkResultDto();

            homeworkResultDto2.Id = 2;
            homeworkResultDto2.Pesel = 22222222222; //pesel drugiego kursanta
            homeworkResultDto2.Result = 90;
            testHomeworkDto.HomeworkResultList.Add(homeworkResultDto2);

            //koniec test course

            //poczatek expected course

            Course expectedCourse = new Course();
            expectedCourse.Id = 1;
            expectedCourse.Name = "Junior C# Devepoler";
            expectedCourse.CourseId = 10;
            expectedCourse.Date = DateTime.Today;
            expectedCourse.HomeworkThreshold = 80;
            expectedCourse.PresenceThreshold = 70;
            expectedCourse.NumberOfStudents = 12;
            expectedCourse.TrainerName = "Jakub Bulczak";

            expectedCourse.CourseStudentList = new List<CourseStudent>();

            //pierwszy kursant
            CourseStudent expectedCourseStudent1 = new CourseStudent();
            expectedCourseStudent1.Id = 2;
            expectedCourseStudent1.Pesel = 11111111111;
            expectedCourse.CourseStudentList.Add(expectedCourseStudent1);

            //drugi kurstant
            CourseStudent expectedCourseStudent2 = new CourseStudent();
            expectedCourseStudent2.Id = 4;
            expectedCourseStudent2.Pesel = 22222222222;
            expectedCourse.CourseStudentList.Add(expectedCourseStudent2);
            
            //lista obecnosci pierwszego kursanta
            expectedCourseStudent1.AbsenceListEachCourseStudent = new List<Absence>();
            Absence expectedAbsence1 = new Absence();
            expectedAbsence1.Id = 1;
            expectedAbsence1.Date = DateTime.Today;
            expectedAbsence1.Presence = Absence.TypeAbsence.n;
            expectedCourseStudent1.AbsenceListEachCourseStudent.Add(expectedAbsence1);

            //lista obecnosci drugiego kursanta
            expectedCourseStudent2.AbsenceListEachCourseStudent = new List<Absence>();
            Absence expectedAbsence2 = new Absence();
            expectedAbsence2.Id = 2;
            expectedAbsence2.Date = DateTime.Today;
            expectedAbsence2.Presence = Absence.TypeAbsence.o;
            expectedCourseStudent2.AbsenceListEachCourseStudent.Add(expectedAbsence2);

            //lista zadan
            expectedCourse.HomeworkList = new List<Homework>();
            Homework expectedHomework = new Homework();
            expectedHomework.Id = 1;
            expectedHomework.MaxResult = 100;

            expectedCourse.HomeworkList.Add(expectedHomework);

            expectedHomework.HomeworkResultList = new List<HomeworkResult>();

            HomeworkResult expectedHomeworkResult1 = new HomeworkResult();
            expectedHomeworkResult1.Id = 1;
            expectedHomeworkResult1.Pesel= 11111111111; //pesel pierwszego kursanta
            expectedHomeworkResult1.Result = 80;
            expectedHomework.HomeworkResultList.Add(expectedHomeworkResult1);


            HomeworkResult expectedHomeworkResult2 = new HomeworkResult();
            expectedHomeworkResult2.Id = 2;
            expectedHomeworkResult2.Pesel = 22222222222; //pesel drugiego kursanta
            expectedHomeworkResult2.Result = 90;
            expectedHomework.HomeworkResultList.Add(expectedHomeworkResult2);

            //wywołanie testowanego Mappera
            Course resultCourse = DtoToEntityMapper.CourseDtoToEntityModel(testCourse);
           
            Assert.AreEqual(expectedCourse.Id,resultCourse.Id);
            Assert.AreEqual(expectedCourse.CourseId,resultCourse.CourseId);
            Assert.AreEqual(expectedCourse.Date,resultCourse.Date);
            Assert.AreEqual(expectedCourse.HomeworkThreshold,resultCourse.HomeworkThreshold);
            Assert.AreEqual(expectedCourse.PresenceThreshold,resultCourse.PresenceThreshold);
            Assert.AreEqual(expectedCourse.NumberOfStudents,resultCourse.NumberOfStudents);
            Assert.AreEqual(expectedCourse.TrainerName,resultCourse.TrainerName);
            Assert.AreEqual(expectedCourse.Name,resultCourse.Name);

            //test kursanta nr 1
            Assert.AreEqual(expectedCourse.CourseStudentList[0].Id, resultCourse.CourseStudentList[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].Pesel, resultCourse.CourseStudentList[0].Pesel);
            //lista obecnosci kursanta nr 1
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Id,resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Date, resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Date);
            Assert.AreEqual(expectedCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Presence,resultCourse.CourseStudentList[0].AbsenceListEachCourseStudent[0].Presence);

            //ogolna lista zadan
            Assert.AreEqual(expectedCourse.HomeworkList[0].Id, resultCourse.HomeworkList[0].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].MaxResult, resultCourse.HomeworkList[0].MaxResult);
            
            //lista wynikow zadan kursanta nt 1
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Id, resultCourse.HomeworkList[0].HomeworkResultList[0].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Pesel,resultCourse.HomeworkList[0].HomeworkResultList[0].Pesel);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[0].Result, resultCourse.HomeworkList[0].HomeworkResultList[0].Result);

            // test kursanta nr 2
            Assert.AreEqual(expectedCourse.CourseStudentList[1].Id, resultCourse.CourseStudentList[1].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].Pesel, resultCourse.CourseStudentList[1].Pesel);
            //lista obecnosci kursanta nr 2 
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Id, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Id);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Date, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Date);
            Assert.AreEqual(expectedCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Presence, resultCourse.CourseStudentList[1].AbsenceListEachCourseStudent[0].Presence);
           
            //lista wynikow zadan kursanta nt 2
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Id, resultCourse.HomeworkList[0].HomeworkResultList[1].Id);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Pesel, resultCourse.HomeworkList[0].HomeworkResultList[1].Pesel);
            Assert.AreEqual(expectedCourse.HomeworkList[0].HomeworkResultList[1].Result, resultCourse.HomeworkList[0].HomeworkResultList[1].Result);


        }
    }
}
