﻿using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;
using CourseRegister.DataLayer.Services;

namespace CourseRegister.DataLayer.NinjectModule
{
   public class RepositoryModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ICourseRegisterUnitOfWork>().To<CourseRegisterUnitOfWork>();
            Bind<ICourseRepositoryService>().To<CourseRepositoryService>();
            Bind<IHomeworkRepositoryService>().To<HomeworkRepositoryService>();
        }
    }
}
