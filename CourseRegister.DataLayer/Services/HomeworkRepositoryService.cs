﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Services
{
    public class HomeworkRepositoryService : IHomeworkRepositoryService
    {
        private GenericRepository<Homework> _homeworkRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;
        public HomeworkRepositoryService(ICourseRegisterUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _homeworkRepository = _unitOfWork.GetRepository<Homework>();
        }

        public IQueryable<Homework>
            GetHomeworkWithHomeworkResultList()
        {
          return _homeworkRepository.GetAll().Include(a => a.HomeworkResultList);
        }
    }
}
