﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Services
{
   public class CourseRepositoryService : ICourseRepositoryService
   {
        private GenericRepository<Course> _courseRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;
        public CourseRepositoryService(ICourseRegisterUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _courseRepository = _unitOfWork.GetRepository<Course>();
        }

       public IQueryable<Course>
            GetCourseWithCourseStudent()
        {
            return _courseRepository.GetAll().Include(a => a.CourseStudentList);
        }
    }
}
