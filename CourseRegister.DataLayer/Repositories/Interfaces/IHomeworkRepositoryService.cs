using System.Linq;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.DataLayer.Repositories.Interfaces
{
    public interface IHomeworkRepositoryService
    {
        IQueryable<Homework>
            GetHomeworkWithHomeworkResultList();
    }
}