﻿namespace CourseRegister.DataLayer.Repositories.Interfaces
{
   public interface IEntity
   {
       int Id { get; set; }
   }
}