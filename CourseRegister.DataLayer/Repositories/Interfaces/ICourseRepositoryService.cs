﻿using System.Linq;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.DataLayer.Repositories.Interfaces
{
    public interface ICourseRepositoryService
    {
        IQueryable<Course>
            GetCourseWithCourseStudent();
    }
}