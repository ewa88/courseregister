namespace CourseRegister.DataLayer.Repositories.Interfaces
{
    public interface ICourseRegisterUnitOfWork
    {
        GenericRepository<T> GetRepository<T>() where T : class, IEntity;
        bool SaveChanges();
    }
}