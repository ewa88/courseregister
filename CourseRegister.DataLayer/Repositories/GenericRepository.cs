﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using CourseRegister.DataLayer.DbContex;
using CourseRegister.DataLayer.Repositories.Interfaces;
using CourseRegister.DataLayer.Services;

namespace CourseRegister.DataLayer.Repositories
{
    public class GenericRepository<T> where T : class, IEntity
    {
        private CourseRegisterDbContext _dbContext;
        private DbSet<T> DataSet => _dbContext.Set<T>();

        public GenericRepository(CourseRegisterDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Add(T entity)
        {
            DataSet.Add(entity);
        }
        public T Get(int id)
        {
            return GetAll().Single(e => e.Id == id);
        }
        public IQueryable<T> GetAll()
        {
            return DataSet; 
        }
        public void Delete(T entity)
        {
            DataSet.Remove(Get(entity.Id));          
        }
        public void Update(T entity)
        {
           // ta metoda nie działa dla edycji studenta
             if (!DataSet.Local.Contains(entity))
            {
                DataSet.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }    
        }
        public void UpdateWithNewContext(T entity)//jedyny sposob jaki znalazlam, ktory zadzialal aby udalo sie dokonac edycji danych studenta
        {
            using (new CourseRegisterDbContext())
            {
                var newEntity = Get(entity.Id);
                newEntity = entity;
                DataSet.AddOrUpdate(newEntity);
            }
        }
    }
}

