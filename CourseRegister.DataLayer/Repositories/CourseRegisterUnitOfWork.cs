﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.DbContex;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Repositories
{
   public class CourseRegisterUnitOfWork : ICourseRegisterUnitOfWork
   {
       private CourseRegisterDbContext _dbContext;
       private Dictionary<Type, object> _repositories;

       public CourseRegisterUnitOfWork()
       {
           _dbContext=new CourseRegisterDbContext();
            RegisterRepositories();

       }
       private void RegisterRepositories()
       {
           _repositories = new Dictionary<Type, object>();
           Register(typeof(Course), new GenericRepository<Course>(_dbContext));
           Register(typeof(Student), new GenericRepository<Student>(_dbContext));
           Register(typeof(CourseStudent), new GenericRepository<CourseStudent>(_dbContext));
           Register(typeof(Absence), new GenericRepository<Absence>(_dbContext));
           Register(typeof(HomeworkResult), new GenericRepository<HomeworkResult>(_dbContext));
           Register(typeof(Homework), new GenericRepository<Homework>(_dbContext));
        }
       private void Register(Type type, object repository)
       {
           _repositories.Add(type, repository);
        }

       public GenericRepository<T> GetRepository<T>() where T : class, IEntity
       {
           return _repositories[typeof(T)] as GenericRepository<T>; 
       }
       public bool SaveChanges()
       {
           int rowsChanged =_dbContext.SaveChanges();
           return rowsChanged != 0;

       }
    }
}
