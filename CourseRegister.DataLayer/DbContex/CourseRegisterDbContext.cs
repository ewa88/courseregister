﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Models;
using System.Configuration;


namespace CourseRegister.DataLayer.DbContex
{
    public class CourseRegisterDbContext : DbContext
    {
        public CourseRegisterDbContext() : base(GetConnectionString())
        {

        }

        public DbSet<Absence> AbsenceDbSet { get; set; }
        public DbSet<Course> CourseDbSet { get; set; }
        public DbSet<CourseStudent> CourseStudentDbSet { get; set; }
        public DbSet<Homework> HomeworkDbSet { get; set; }
        public DbSet<Student> StudentDbSet { get; set; }




        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabaseEwaRudnik"].ConnectionString;
        }
    }

}
