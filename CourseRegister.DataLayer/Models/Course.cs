﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Models
{
    public class Course:IEntity
    {
    public int Id { get; set; }
    public int CourseId { get; set; }
    public string Name { get; set; }
    public string TrainerName { get; set; }
    public DateTime Date { get; set; }
    public int HomeworkThreshold { get; set; }
    public int PresenceThreshold { get; set; }
    public int NumberOfStudents { get; set; }

    public virtual List<CourseStudent> CourseStudentList { get; set; }
 //   public virtual  List<Homework> HomeworkList { get; set; }
    }
}
