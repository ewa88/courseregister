﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Models
{
    public class Absence:IEntity
    {
        public enum TypeAbsence
        {
            o,
            n
        }

        public DateTime Date { get; set; }
        public TypeAbsence Presence { get; set; }
        public int Id { get; set; }
        public int CourseStudentId { get; set; }

    }
}
