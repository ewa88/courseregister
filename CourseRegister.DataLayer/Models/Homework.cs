﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Models
{
   public class Homework:IEntity
    {
        
        public int Id { get; set; }
        public int MaxResult { get; set; }
        public int CourseId { get; set; }

        public virtual List<HomeworkResult> HomeworkResultList { get; set; }
    }
}
