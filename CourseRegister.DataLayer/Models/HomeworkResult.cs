﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Models
{
   public class HomeworkResult:IEntity
    {
        
        public int Id { get; set; }
        public int Result { get; set; }
        public long Pesel { get; set; }
}
}
