﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.DataLayer.Models
{
    public class Student:IEntity
    {
        public enum TypeSex
        {
            K,
            M
        }
        [Required]
        public long PESEL { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public TypeSex Sex { get; set; }
        public int Id { get; set; }
    }
}
