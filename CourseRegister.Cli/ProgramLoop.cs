﻿using System;
using System.Collections.Generic;
using CourseRegister.Cli.IoHelpers;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.BusinessLayer.Service;
using CourseRegister.Cli.Interface;
using CourseRegister.Cli.JsonExport;

namespace CourseRegister.Cli
{
    public class ProgramLoop : IProgramLoop
    {     
        private ICourseServices _courseServices;
        private IStudentService _studentService;
        private IAbsenceService _absenceService;
        private IHomeworkService _homeworkService;
        private IReportGeneration _reportGeneration;
        private IEditData _editData;
        public int ActiveCourseId;
        public ProgramLoop(ICourseServices courseServices,IStudentService studentService, 
            IAbsenceService absenceService, IReportGeneration reportGeneration, IEditData editData, IHomeworkService homeworkService)
        {
            _courseServices = courseServices;
            _studentService = studentService;
            _absenceService = absenceService;
            _homeworkService = homeworkService;
            _reportGeneration = reportGeneration;
            _editData = editData;
        }
        public void StartProgram()
        {
           var commandDispatcher = new CommandDispatcher();
           var exportJson = new ExportJson();
           bool loop = true;
           int courseId = 0;
           AddCommandsToDictionary(commandDispatcher);
           _reportGeneration.ReportGenerationFinished += exportJson.PrintJson;
            while (loop)
            {
                string answer = ConsoleReadHelper.GetAnswer("Wybierz polecenie dla dziennika (addStudents/addCourse/" +
                                                                     "addPresence/addHomework/courseReport/changeActiveCourse/editCourse/editStudent/exit):");
                if (answer == "exit" || answer == "Exit")
                {
                    loop = false;
                }
                else
                {
                   courseId=commandDispatcher.ExecuteCommand(answer, courseId);
                }
            }
        }     
        public void AddCommandsToDictionary(CommandDispatcher commandDispatcher)
        {          
            commandDispatcher.AddCommand("addStudents", AddStudent);
            commandDispatcher.AddCommand("addCourse", AddCourse);
            commandDispatcher.AddCommand("addPresence", AddPresence);
            commandDispatcher.AddCommand("addHomework", AddHomework);
            commandDispatcher.AddCommand("courseReport", _reportGeneration.Report);
            commandDispatcher.AddCommand("changeActiveCourse", ChangeActiveCourse);
            commandDispatcher.AddCommand("editCourse", _editData.EditCourse);
            commandDispatcher.AddCommand("editStudent", _editData.EditStudent);
        }
        public int AddCourse(int courseId)
        {
            bool loop = true;
            string answerExit;
            int lastAddedCourseId = courseId;
            try
            {
                while (loop)
                {
                    CourseDto course = new CourseDto();

                    answerExit = ConsoleReadHelper.GetAnswer(
                        "Podaj nr porzadkowy kursu (jesli chcesz zaprzestac dodawania kursow wpisz exit): ");

                    if (answerExit == "exit" || answerExit == "Exit")
                    {
                        loop = false;
                    }
                    else
                    {
                        course.CourseId = Int32.Parse(answerExit);
                        if (_courseServices.CheckIfCourseExist(course.CourseId))
                        {
                            Console.WriteLine("Kurs o podanym Id juz istnieje. ");
                        }
                        else
                        {
                            course.Name = ConsoleReadHelper.GetCourseNameUsingRegex
                                ("Podaj nazwe kursu zgodną ze wzorcem: C#_<dowolna nazwa złożona z liter, cyfr lub _ > _ < dwie duże litery > :");
                            course.TrainerName = ConsoleReadHelper.GetAnswer("Podaj imie i nazwisko trenera: ");
                            course.Date = ConsoleReadHelper.GetData();
                            course.HomeworkThreshold =
                                ConsoleReadHelper.GetNumber("Podaj prog zaliczenia w procentach dla prac domowych:");
                            course.PresenceThreshold =
                                ConsoleReadHelper.GetNumber("Podaj prog dla obecnosci kursanta (w procentach): ");
                            course.NumberOfStudents = ConsoleReadHelper.GetNumber("Podaj liczbe kursantow w grupie: ");

                            course.CourseStudentList = new List<CourseStudentDto>();

                            bool properPesel = false;
                            while (properPesel == false) 
                            {
                                long answerPesel = ConsoleReadHelper.GetPesel();
                                if (answerPesel == 0) 
                                {
                                    properPesel = true;
                                }
                                else
                                {
                                    bool checkStudentExist = false;
                                    foreach (var student in course.CourseStudentList)
                                    {
                                        if (student.Pesel == answerPesel)
                                        {

                                            Console.WriteLine(
                                                "Podany PESEL zostal juz przypisany do tego kursu. ");
                                            checkStudentExist = true;
                                        }
                                    }
                                    if (checkStudentExist == false)
                                    {
                                        if (_studentService.CheckIfStudentExistByPesel(answerPesel))
                                        {
                                            CourseStudentDto courseStudent = new CourseStudentDto();
                                            courseStudent.Pesel = answerPesel;
                                         
                                            course.CourseStudentList.Add(courseStudent);
                                            Console.WriteLine("Dodano studenta. ");
                                            if (course.NumberOfStudents == course.CourseStudentList.Count)
                                            {
                                                Console.WriteLine("Podano juz zadeklarowana liczbę kursantow. ");
                                                properPesel = true;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine(
                                                "Wprowadzono PESEL kursanta, ktorego nie ma w bazie. ");
                                        }
                                    }

                                }
                            }
                           
                            if (_courseServices.AddCourse(course))
                            {
                                Console.WriteLine("Pomyslnie dodano kurs. ");
                                lastAddedCourseId = course.CourseId;
                                Console.WriteLine("Dodano kurs o numerze: " + lastAddedCourseId);
                            }
                            else
                            {
                                Console.WriteLine("Nie dodano kursu. ");
                            }
                        }
                    }
                }
        }
            catch (Exception e)
            {
             Console.WriteLine("Blad podawanych danych");
            }
            return lastAddedCourseId;
        }

        public int AddStudent(int courseId)
        {
            bool jumpFromAddStudent = true;
            try
            {

                bool properPesel = false;
                while (properPesel == false)
                {
                    long answerPesel = ConsoleReadHelper.GetPesel();
                    if (answerPesel == 0) //jesli jest rowne 0 to znaczy ze ktos wpisal exit
                    {
                        properPesel = true;
                    }
                    else
                    {;
                        if (_studentService.CheckIfStudentExistByPesel(answerPesel))
                        {
                            Console.WriteLine("Blad. Student o podanym PESELu zostal juz wprowadzony. ");
                        }
                        else
                        {
                            StudentDto student = new StudentDto();
                            student.PESEL = answerPesel;
                            student.Name = ConsoleReadHelper.GetAnswer("Podaj imie kursanta: ");
                            student.Surname = ConsoleReadHelper.GetAnswer("Podaj nazwisko kursanta: ");
                            student.BirthDate = ConsoleReadHelper.GetData("Podaj date urodzenia kursanta: ");
                            string sex = ConsoleReadHelper.GetSex();
                            student.Sex = (StudentDto.TypeSex) Enum.Parse(typeof(StudentDto.TypeSex), (sex.ToUpper()));

                            if (_studentService.AddStudent(student))
                            {
                                Console.WriteLine("Operacja udana");
                            }
                            else
                            {
                                Console.WriteLine("Operacja nie powiodla sie.");

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Blad podawanych danych");
           }
            return courseId;
        }

        public int AddPresence(int activCourseId)
        {
            if (_courseServices.CheckIfAnyCourseExist())
            {
                try
                {
                int courseId = activCourseId;
                    if (_courseServices.CheckIfCourseExist(courseId))
                    {
                        DateTime date = DateTime.Now;
                        date = ConsoleReadHelper.GetData("Podaj date: ");
                        foreach (var courseStudentDto in _courseServices.GetStudentsByCourseId(courseId))
                        {

                            AbsenceDto absence = new AbsenceDto();
                            absence.CourseStudentId = courseStudentDto.Id;
                            absence.Date = date;
                            Console.WriteLine("Wpisz obecnosc (o-obecny, n-nieobecny) dla kursatna o numerze: " +
                                              courseStudentDto.Pesel);
                            absence.Presence =
                                (AbsenceDto.TypeAbsence) Enum.Parse(typeof(AbsenceDto.TypeAbsence),
                                    (Console.ReadLine()));
                            if(_absenceService.AddAbsenceToStudentsList(absence))
                        {
                            Console.WriteLine("Pomyslnie dodano obecnosc do bazy danych. ");
                        }

                        }
                    }
                    else
                    {
                        Console.WriteLine(
                            "Obecnie zaden kurs nie jest aktywny, uzyj komendy changeActiveCourse i okresl aktywny kurs");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Blad podawanych danych");
                }
            }
            else
            {
                Console.WriteLine("Blad. Nie dodano studentow lub kursu.");
            }
            return activCourseId;
        }

        public int AddHomework(int activCourseId)
        {
            if (_courseServices.CheckIfAnyCourseExist())
            {
               try
                {
                int courseId = activCourseId;
                    if (_courseServices.CheckIfCourseExist(courseId))
                    {
                        HomeworkDto homework = new HomeworkDto();
                        homework.CourseId = courseId;
                        homework.MaxResult =
                            ConsoleReadHelper.GetNumber(
                                "Podaj maksymalna liczbe punktow do uzyskania z pracy domowej: ");

                        homework.HomeworkResultList = new List<HomeworkResultDto>();

                        int wynik = 0;

                        var studentsList = _courseServices.GetStudentsByCourseId(courseId);

                        foreach (var student in studentsList)
                        {
                           
                        Console.WriteLine("Podaj ilosc punktow uzyskanych przez studenta o numerze PESEL: " +
                                              student.Pesel);
                            wynik = Int32.Parse(Console.ReadLine());
                            HomeworkResultDto homeworkResult = new HomeworkResultDto();
                            homeworkResult.Pesel = student.Pesel;
                            homeworkResult.Result = wynik;
                        
                            homework.HomeworkResultList.Add(homeworkResult);
                        
                        }
                        if(_homeworkService.AddHomework(homework))
                        {
                            Console.WriteLine("Pomyslnie dokonano zmian w bazie danych ");
                        }
                    }
                    else
                    {
                        Console.WriteLine(
                            "Obecnie zaden kurs nie jest aktywny, uzyj komendy changeActiveCourse i okresl aktywny kurs");
                    }
                }
                catch (Exception e)
                {
                   Console.WriteLine("Blad podawanych danych");
               }
            }
            else
            {
                Console.WriteLine("Blad. Nie dodano studentow lub kursu.");
            }
            return activCourseId;
        }
        public int ChangeActiveCourse(int activCourse)
        {
            int courseId = activCourse;
            bool loop = true;
            while (loop)
            {
                courseId = ConsoleReadHelper.GetNumber(
                    "Podaj Id kursu, ktory ma byc aktywny: ");
                if (_courseServices.CheckIfCourseExist(courseId))
                {
                    loop = false;
                    Console.WriteLine("Aktuaknie aktywny kurs: " + courseId);
                }
                else
                {
                    Console.WriteLine("Podany kurs nie istnieje. ");
                }
            }
            return courseId;
        }
    }
}















