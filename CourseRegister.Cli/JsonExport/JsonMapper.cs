﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace CourseRegister.Cli.JsonExport
{
    public class JsonMapper
    {
        public string FromContent(ReportData report)
        {
            return JsonConvert.SerializeObject(report,Formatting.Indented);
        }
    }
}
