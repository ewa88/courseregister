﻿using System.IO;

namespace CourseRegister.Cli.JsonExport
{
   public class FileRepository<T>
    {
        private JsonMapper jsonMapper;

        public FileRepository(JsonMapper jsonMapper)
        {
            this.jsonMapper = jsonMapper;
        }
        public void Save(string filePath,  ReportData report)
        {
            File.WriteAllText(filePath, jsonMapper.FromContent(report));
        }
       
    }
}