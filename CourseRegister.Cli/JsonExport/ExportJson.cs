﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.Cli.IoHelpers;

namespace CourseRegister.Cli.JsonExport
{
    public class ExportJson
    {
        public void PrintJson(object sender, ReportEventArgs arg)
        {
            string fileName = ConsoleReadHelper.GetAnswer("Wpisz nazwe pliku Json.");
            if (!fileName.EndsWith(".json"))
            {
                fileName = fileName + ".json";
            }

            //string filepath = @"C:\Users\Student13\Desktop\Homework\Homework_ETAP2_TK_1\" + name; sciezka bezwgledna
            string filepath = @"..\..\..\" + fileName;//sciezka wzgledna
            var fileRepository = new FileRepository<ReportData>(new JsonMapper());//wywołanie z Jsona
            fileRepository.Save(filepath, arg.ReportData);
        }
    }
}
