﻿using System;
using System.Collections.Generic;

namespace CourseRegister.Cli.JsonExport
{
    public class ReportData
    {
        public int CourseId;
        public string CourseName;
        public DateTime StartDate;
        public string TrainerName;
        public int PresenceThreshold;
        public int HomeworkThreshold;

        public List<StudentRaport> StudentsList = new List<StudentRaport>();
    }
    public class StudentRaport
    {
        public string StudentName;
        public string StudentSurname;
        public int PresenceResult;
        public float HomeworkResult;
    }
        
        


}
