﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.Cli.Interface;
using CourseRegister.Cli.IoHelpers;
using CourseRegister.Cli.JsonExport;
using Ninject.Infrastructure.Language;

namespace CourseRegister.Cli
{
   public class CommandDispatcher
   {
       public delegate int CommandDelegationHandler(int courseId);

       private readonly Dictionary<string,CommandDelegationHandler> _commandDictionary = new Dictionary<string, CommandDelegationHandler>();

       public bool CheckIfCommandExists(string command)
       {
           return _commandDictionary.ContainsKey(command);
       }

        public void AddCommand(string command, CommandDelegationHandler commandHandler)
       {
           _commandDictionary.Add(command,commandHandler);
       }

       public int ExecuteCommand(string command, int courseId)
       {
           if (!CheckIfCommandExists(command))
           {
               Console.WriteLine("Podana komenda nie istnieje, wpisz ja ponownie. ");
               return courseId;
           }
           else
           {
              var handler = _commandDictionary[command];
              return handler(courseId);
            }
         
        }      
    }
   }

