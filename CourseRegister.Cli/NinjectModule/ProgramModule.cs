﻿using CourseRegister.Cli.Interface;

namespace CourseRegister.Cli.NinjectModule
{
    public class ProgramModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IProgramLoop>().To<ProgramLoop>();
            Bind<IReportGeneration>().To<ReportGeneration>();
            Bind<IEditData>().To<EditData>();
        }
    }
}
