﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.Cli.Interface;
using CourseRegister.Cli.IoHelpers;

namespace CourseRegister.Cli
{
    public class EditData : IEditData
    {
        private IStudentService _studentService;
        private ICourseServices _courseServices;

        public EditData(IStudentService studentService, ICourseServices courseServices)
        {
            _studentService = studentService;
            _courseServices = courseServices;
        }

        public int EditStudent(int courseId)
        {
            bool loop = true;
            while (loop)
            {
                Console.WriteLine("Studenci w bazie danych: ");
                _studentService.GetAllStudents();
                foreach (var student in _studentService.GetAllStudents())
                {
                    Console.Write("Id: " + student.Id + ", ");
                    Console.Write("imie: " + student.Name + ", ");
                    Console.Write("nazwisko: " + student.Surname + ".\n ");
                }

                int id = ConsoleReadHelper.GetNumber("Wybierz Id studenta do edycji danych: ");
                if (!_studentService.CheckIfStudentExistById(id))
                {
                    Console.WriteLine("Blad. Student o podanym Id nie istnieje. ");
                }
                else
                {
                    var student = _studentService.GetStudentById(id);
                    string newName = ConsoleReadHelper.GetAnswer("Edycja imienia: ");
                    if (!String.IsNullOrEmpty(newName))
                    {
                        student.Name = newName;
                    }
                    string newSurname = ConsoleReadHelper.GetAnswer("Edycja nazwiska: ");
                    if (!String.IsNullOrEmpty(newSurname))
                    {
                        student.Surname = newSurname;
                    }
                    string birthdate = "";
                    bool rightDate = false;
                    while (rightDate == false)
                    {
                        birthdate = ConsoleReadHelper.GetAnswer("Edycja daty: ");
                        try
                        {
                            if (birthdate != "")
                            {
                                student.BirthDate = DateTime.Parse(birthdate);
                                rightDate = true;
                            }
                            else
                            {
                                rightDate = true;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Blednie wpisano date. ");
                            rightDate = false;
                        }
                    }
                    if (_studentService.UpdateStudent(student))
                    {
                        Console.WriteLine("Pomyslnie dokonano zmiany  w bazie danych. \n");
                    }
                    StudentDto studentDto = _studentService.GetStudentById(student.Id);

                    foreach (var course in _courseServices.GetAllCourses())
                    {
                        foreach (var courseStudent in course.CourseStudentList)
                        {
                            if (courseStudent.Pesel == studentDto.PESEL)
                            {
                                Console.Write("Id " + courseStudent.Id + ", ");
                                Console.Write("kurs: " + course.Name + ", ");
                                Console.WriteLine("");
                            }
                        }

                    }
                    if (_courseServices.CheckIfCourseStudentExist(studentDto.PESEL))
                    {
                        Console.WriteLine(studentDto.Name + " " + studentDto.Surname +
                                          " nie jest przypisany do żadnego kursu (nie można usunac z kursu). \n");
                        loop = false;

                    }
                    else
                    {


                        string answerRemove;
                        answerRemove = ConsoleReadHelper.GetAnswer(
                            "\nWpisz Id kursanta, aby wypisac go z kursu (jesli chcesz zakonczyc wpisz exit). ");
                        if (answerRemove == "exit" || answerRemove == "Exit")
                        {
                            loop = false;
                        }
                        else
                        {
                           
                            int studentRemoveId = Int32.Parse(answerRemove);

                            long studentRemovePesel = _courseServices.GetCourseStudentPeselByCourseStudentId(studentRemoveId);

                                if (_courseServices.CourseStudentRemove(studentRemoveId, courseId, studentRemovePesel))
                            {
                                Console.WriteLine("Student zostal wypisany z kursu. ");

                            }
                            loop = false;
                        }
                    }
                }
            }
            return courseId;
        }


        public int EditCourse(int activCourseId)
        {
            int courseId = activCourseId;
            if (_courseServices.CheckIfCourseExist(courseId))
            {
                bool loop = true;
                while (loop)
                {
                    var courseDto = new CourseDto();
                    if (_courseServices.CheckIfCourseExist(courseId))
                    {
                        List<CourseDto> courseDtoList = _courseServices.GetCourseById(courseId);
                        Console.WriteLine("Aktualnie aktywny kurs: \n");
                        foreach (var course in courseDtoList)
                        {
                            Console.WriteLine("Id kursu: " + course.CourseId);
                            Console.WriteLine("Nazwa kursu: " + course.Name);
                            Console.WriteLine("Trener: " + course.TrainerName);
                            Console.WriteLine("Prog dla obecnosci: " + course.PresenceThreshold);
                            Console.WriteLine("Prog dla prac domowych: " + course.HomeworkThreshold);
                            Console.WriteLine("");
                            if (course.CourseStudentList.Count == 0)
                            {
                                Console.WriteLine("Brak przypisanych kursantów do kursu. ");
                            }
                            else
                            {
                                Console.WriteLine("Lista kursantow: ");
                                foreach (var courseStudent in course.CourseStudentList)
                                {
                                    StudentDto studentDto = _studentService.GetStudentByPesel(courseStudent.Pesel);
                                    Console.Write("id: " + courseStudent.Id + ", ");
                                    Console.Write(studentDto.Name + ", ");
                                    Console.Write(studentDto.Surname + "\n");
                                }
                                Console.WriteLine("");
                            }
                        }
                        string courseName, courseTrainer;
                        int answerPresence, answerHomework;
                        courseDto = _courseServices.GetCourseDtoById(courseId);
                        courseName = ConsoleReadHelper.GetAnswer("Edycja nazwy: ");
                        if (!String.IsNullOrEmpty(courseName))
                        {
                            bool nameLoop = true;
                            while (nameLoop)
                            {

                                Match match = Regex.Match(courseName, @"^C#_\w+_[A-Z]{2}$");
                                if (match.Success)
                                {
                                    nameLoop = false;
                                    courseDto.Name = courseName;
                                }
                                else
                                {
                                    courseName =
                                        ConsoleReadHelper.GetAnswer(
                                            "Podaj nazwe kursu zgodną ze wzorcem: C#_<dowolna nazwa złożona z liter, cyfr lub _ > _ < dwie duże litery > :");
                                }
                            }

                        }
                        courseTrainer = ConsoleReadHelper.GetAnswer("Edycja danych trenera: ");
                        if (!String.IsNullOrEmpty(courseTrainer))
                        {
                            courseDto.TrainerName = courseTrainer;
                        }
                        answerPresence = ConsoleReadHelper.GetInt("Edycja progu dla obecnosci: ");

                        if (answerPresence != -1)
                        {
                            courseDto.PresenceThreshold = answerPresence;

                        }
                        answerHomework = ConsoleReadHelper.GetInt("Edycja progu dla zadan domowych: ");
                        if (answerHomework != -1)
                        {
                            courseDto.HomeworkThreshold = answerHomework;
                        }

                        if (_courseServices.UpdateCourse(courseDto))
                        {
                            Console.WriteLine("Pomyslnie dokonano zmiany w bazie danych. ");
                           
                        }
                        if (courseDto.CourseStudentList.Count == 0)
                        {
                            loop = false;
                        }
                        else
                        {
                        bool removeLoop=true;
                            while (removeLoop)
                            {
                                string answerRemove = ConsoleReadHelper.GetAnswer(
                                    "Wpisz Id studenta, aby wypisac go z kursu (jesli chcesz zakonczyc wpisz exit. ");
                                if (answerRemove == "exit" || answerRemove == "Exit")
                                {
                                    removeLoop = false;
                                    loop = false;
                                }
                                else
                                {
                                    int studentRemoveId;
                                    long courseStudentPesel=0;
                                    bool succes = Int32.TryParse(answerRemove, out studentRemoveId);
                                    if (succes)
                                    {
                                        foreach (var student in courseDto.CourseStudentList)
                                        {
                                            if (student.Id == studentRemoveId)
                                            {
                                                courseStudentPesel = student.Pesel;
                                            }
                                        }
                                        if (!_courseServices.CheckIfCourseStudentExistById(studentRemoveId))
                                        {
                                            Console.WriteLine(
                                                "Kursant o taki Id nie został przypisany do wybranego kursu.");

                                        }
                                        else
                                        {
                                            if (_courseServices.CourseStudentRemove(studentRemoveId, courseId,courseStudentPesel ))
                                                Console.WriteLine("Student zostal wypisany z kursu. ");

                                            if (!_courseServices.CheckIfCourseHasNoStudents(courseId))
                                            {
                                                Console.WriteLine("Wszyscy studenci zostali wypisani z kursu.");
                                                removeLoop = false;
                                                loop = false;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                }
                    else
                    {
                        Console.WriteLine(
                            "Obecnie zaden kurs nie jest aktywny, uzyj komendy changeActiveCourse i okresl aktywny kurs");

                    }
                    Console.WriteLine("");

                }
            }
            return courseId;


        }
    }
}

