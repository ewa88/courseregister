﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Service;

namespace CourseRegister.Cli.IoHelpers
{
    internal class ConsoleWriteHelper
    {
        public static void PrintCommand(string command)//w nawiasie podaje co ma wyswietlic
        {
            Console.WriteLine(command);
        }

        public static void Separation()
        {
            Console.WriteLine("");
        }

        public static void WriteCourseData(CourseDto course)
        {
            Console.WriteLine("Id kursu: " + course.CourseId);
            Console.WriteLine("Nazwa kursu: " + course.Name);
            Console.WriteLine("Data rozpoczecia kursu: " + course.Date);
            Console.WriteLine("Trener: " + course.TrainerName);
            Console.WriteLine("Prog dla obecnosci: " + course.PresenceThreshold);
            Console.WriteLine("Prog dla prac domowych: " + course.HomeworkThreshold);
        }

        public static void PrintStudentPresenceData(StudentDto studentDto, int n, int y, int wynik, string z)
        {
            Console.Write(studentDto.Name + " ");
            Console.Write(studentDto.Surname + " ");
            Console.WriteLine(n + "/" + y + " (" + wynik + "%) - " + z);
        }

        public static void PrintStudentHomeworkData(StudentDto studentDto, int sumResult, int maxResult, float procent,string k)
        {
            Console.Write(studentDto.Name + " ");
            Console.Write(studentDto.Surname + " ");
            Console.WriteLine(sumResult + "/" +
                              maxResult + " (" + procent + "%) " + " - " + k);
        }
    }
}
