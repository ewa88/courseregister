﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;

namespace CourseRegister.Cli.IoHelpers
{
    internal class ConsoleReadHelper
    {
        public static string CommandTypes()
        {
            string answer;
            Console.WriteLine("Wybierz polecenie dla dziennika (addStudents/addCourse/" +
                              "addPresence/addHomework/courseReport/changeActiveCourse/editCourse/editStudent/exit):");
            answer = Console.ReadLine();
            return answer;
        }

        public static string GetAnswer(string message)
        {
            string answer;
            Console.WriteLine(message);
            answer = Console.ReadLine();
            return answer;
        }

        public static string GetCourseNameUsingRegex(string message)
        {
            string answer = "";
            bool loop=true;
            while (loop)
            {
                Console.WriteLine(message);
                answer = Console.ReadLine();
                Match match = Regex.Match(answer, @"^C#_\w+_[A-Z]{2}$");
                if (match.Success)
                {
                   loop = false;
                }
            }
            return answer;
        }

        public static DateTime GetData()
        {
            string answerDate;
            DateTime date = DateTime.Parse("1999/01/01");

            bool properCourseDate = false;

            while (properCourseDate == false)
            {
                Console.WriteLine("Podaj date rozpoczecia kursu: ");
                answerDate = Console.ReadLine();
                try
                {
                    date = DateTime.Parse(answerDate);
                    properCourseDate = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Blednie wprowadzono date.");
                    properCourseDate = false;
                }

            }
            return date;
        }
        public static DateTime GetBirthData()
        {
            string answerDate="";
            DateTime date = DateTime.Parse("1999/01/01");

            bool properCourseDate = false;

            while (properCourseDate == false)
            {
                Console.WriteLine("Edycja daty urodzenia: ");
                answerDate = Console.ReadLine();
                
                try
                {
                    date = DateTime.Parse(answerDate);
                    properCourseDate = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Blednie wprowadzono date.");
                    properCourseDate = false;
                }

            }
            return date;
        }

        public static int GetNumber(string message)
        {
            int number;
            Console.WriteLine(message);


            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Nie podano prawidlowej liczby. ");
            }
            return number;
        }

        public static int GetInt(string message)
        {
            bool properInt = false;
            string answer;
            int parseAnswer = 0;
            while (properInt == false)
            {
                Console.WriteLine(message);
                answer = Console.ReadLine();
                if (!String.IsNullOrEmpty(answer))
                {

                    try
                    {
                        parseAnswer = Int32.Parse(answer);

                        properInt = true;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Blad. Wpisz dane ponownie.");
                        properInt = false;
                    }
                }
                else
                {
                    properInt = true;
                    parseAnswer = -1;
                }
            }
            return parseAnswer;
        }

        public static long GetPesel()
        {

            bool properpesel = false;
            string answer="";

            

            while (properpesel == false)
            {
                Console.WriteLine(
                    "Wprowadz PESEL uczestnika kursu (jesli chcesz zakonczyc wpisz exit) : ");

                answer = Console.ReadLine();
                if (answer == "exit" || answer == "Exit")
                {
                    return 0;//metoda zwroci 0 jesli wpisano exit, 0 jest wyjsciem z petli
                }
                if (long.Parse(answer) <= 100000000 || long.Parse(answer) >= 99999999999)
                {
                    Console.WriteLine("Blad. Wpisano niepoprawny PESEL. ");
                }
                else
                {
                    properpesel = true;

                }
            }
            return long.Parse(answer);
        }

        public static DateTime GetData(string message)
        {
            string answerDate;
            DateTime date = DateTime.Parse("1999/01/01");

            bool properCourseDate = false;

            while (properCourseDate == false)
            {
                Console.WriteLine(message);
                answerDate = Console.ReadLine();
                try
                {
                    date = DateTime.Parse(answerDate);
                    properCourseDate = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Blednie wprowadzono date.");
                    properCourseDate = false;
                }

            }
            return date;
        }
        
        public static string GetSex()
        {
            string chooseSex;
            Console.WriteLine("Podaj płeć kursanta(K/M): ");
            string sex = Console.ReadLine();

            return sex;
        }
    }
}






    
