﻿using CourseRegister.BusinessLayer.NinjectModules;
using CourseRegister.Cli.NinjectModule;
using CourseRegister.DataLayer.NinjectModule;
using Ninject;

namespace CourseRegister.Cli
{
    internal class Program
    {
       private static void Main()
        {
            IKernel kernel = new StandardKernel(new ServiceModule(), new RepositoryModule(), new ProgramModule());

            kernel.Get<ProgramLoop>().StartProgram();
               
        }
    }
}
