﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.Cli.Interface;
using CourseRegister.Cli.IoHelpers;
using CourseRegister.Cli.JsonExport;

namespace CourseRegister.Cli
{
    public class ReportGeneration : IReportGeneration
    {
        private ICourseServices _courseServices;
        private IAbsenceService _absenceService;
        private IStudentService _studentService;
        private IHomeworkService _homeworkService;

        public ReportGeneration(ICourseServices courseServices, IAbsenceService absenceService, IStudentService studentService,IHomeworkService homeworkService)
        {
            _courseServices = courseServices;
            _absenceService = absenceService;
            _studentService = studentService;
            _homeworkService = homeworkService;

        }
        public delegate void ReportGenerationFinishedEventHandler(object sender, ReportEventArgs e);

        public event ReportGenerationFinishedEventHandler ReportGenerationFinished;

        public void OnReportFinished(ReportData reportData)
        {
            if (ReportGenerationFinished == null)
            {
                return;
            }
            var eventArgs = new ReportEventArgs();
            eventArgs.ReportData = reportData;

            ReportGenerationFinished(this, eventArgs);
        }
      
        public int Report(int activCourseId)
        {
            var report = new ReportData();
            int courseId = activCourseId;
            if (_courseServices.CheckIfCourseExist(courseId))
            {
                List<CourseDto> courseDtoList = _courseServices.GetCourseById(courseId);
                foreach (var course in courseDtoList)
                {
                    ConsoleWriteHelper.WriteCourseData(course);
                    report.CourseId = course.CourseId;
                    report.CourseName = course.Name;
                    report.StartDate = course.Date;
                    report.TrainerName = course.TrainerName;
                    report.PresenceThreshold = course.PresenceThreshold;
                    report.HomeworkThreshold = course.HomeworkThreshold;
                    if (course.CourseStudentList.Count > 0)
                        ConsoleWriteHelper.PrintCommand("\nPresence: ");
                    foreach (var courseStudent in course.CourseStudentList)
                    {
                        int n = _absenceService.GetPresenceDaysNumber(courseStudent);
                        int y = _absenceService.GetDaysNumber(courseStudent);
                        int wynik;
                        string z;
                        if (y == 0)
                        {
                            wynik = 0;
                            z = "Niezaliczone";
                        }
                        else
                        {
                            wynik = n * 100 / y;
                            if (wynik >= course.HomeworkThreshold)
                            {
                                z = "Zaliczone";
                            }
                            else
                            {
                                z = "Niezaliczone";
                            }
                        }
                        StudentDto studentDto = _studentService.GetStudentByPesel(courseStudent.Pesel);
                        ConsoleWriteHelper.PrintStudentPresenceData(studentDto,n,y,wynik,z);
                    }
                    if (course.CourseStudentList.Count > 0)
                        ConsoleWriteHelper.PrintCommand("\nHomework: ");
                    foreach (var courseStudent in course.CourseStudentList)
                    {
                        float procent;
                        string k;
                        if (_homeworkService.MaxResult(course) == 0)
                        {
                            procent = 0;
                            k = "Niezaliczone";
                        }
                        else
                        {
                            procent = _homeworkService.SumResult(course, courseStudent) * 100 /
                                      _homeworkService.MaxResult(course);

                            if (procent >= course.HomeworkThreshold)
                            {
                                k = "Zaliczone";
                            }
                            else
                            {
                                k = "Niezaliczone";
                            }
                        }
                        StudentDto studentDto = _studentService.GetStudentByPesel(courseStudent.Pesel);
                        ConsoleWriteHelper.PrintStudentHomeworkData(studentDto,
                            _homeworkService.SumResult(course, courseStudent), _homeworkService.MaxResult(course),
                            procent, k);
                    }
                    foreach (var thisCourseStudent in course.CourseStudentList)
                    {
                        var student = new StudentRaport();
                        int daysPresence = _absenceService.GetPresenceDaysNumber(thisCourseStudent);
                        int allDays = _absenceService.GetDaysNumber(thisCourseStudent);
                        int result;
                        if (allDays == 0)
                        {
                            result = 0;
                        }
                        else
                        {
                            result = daysPresence * 100 / allDays;
                        }
                        StudentDto thisStudent = _studentService.GetStudentByPesel(thisCourseStudent.Pesel);
                        student.StudentName = thisStudent.Name;
                        student.StudentSurname = thisStudent.Surname;
                        student.PresenceResult = result;
                        float homeworkResult;
                        if (_homeworkService.MaxResult(course) == 0)
                        {
                            homeworkResult = 0;
                        }
                        else
                        {
                            homeworkResult = _homeworkService.SumResult(course, thisCourseStudent) * 100 /
                                             _homeworkService.MaxResult(course);
                        }
                        student.HomeworkResult = homeworkResult;
                        report.StudentsList.Add(student);
                    }
                }
                ConsoleWriteHelper.Separation();
                OnReportFinished(report);
            }
            else
            {
                ConsoleWriteHelper.PrintCommand(
                    "Obecnie zaden kurs nie jest aktywny, uzyj komendy changeActiveCourse i okresl aktywny kurs");
            }
            ConsoleWriteHelper.Separation();

            return activCourseId;
        }

    }
}
