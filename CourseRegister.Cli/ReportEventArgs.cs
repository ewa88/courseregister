﻿using System;
using CourseRegister.Cli.JsonExport;

namespace CourseRegister.Cli
{
    public class ReportEventArgs : EventArgs
    {
        public ReportData ReportData;
    }
}