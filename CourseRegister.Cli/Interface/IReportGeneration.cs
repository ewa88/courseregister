using CourseRegister.Cli.JsonExport;

namespace CourseRegister.Cli.Interface
{
    public interface IReportGeneration
    {
        event ReportGeneration.ReportGenerationFinishedEventHandler ReportGenerationFinished;
        void OnReportFinished(ReportData reportData);
        int Report(int activCourseId);
    }
}