namespace CourseRegister.Cli.Interface
{
    public interface IEditData
    {
        int EditStudent(int courseId);
        int EditCourse(int activCourseId);
    }
}