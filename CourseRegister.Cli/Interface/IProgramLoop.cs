namespace CourseRegister.Cli.Interface
{
    public interface IProgramLoop
    {
        int AddCourse(int courseId);
        int AddStudent(int courseId);
        int AddPresence(int activCourseId);
        int AddHomework(int activCourseId);
        int ChangeActiveCourse(int activCourse);
    }
}