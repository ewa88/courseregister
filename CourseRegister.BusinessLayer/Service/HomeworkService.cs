﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories.Interfaces;
using CourseRegister.DataLayer.Services;


namespace CourseRegister.BusinessLayer.Service
{
    public class HomeworkService : IHomeworkService
    {
        private GenericRepository<Homework> _homeworkRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;

        public HomeworkService(ICourseRegisterUnitOfWork courseRegisterUnitOfWork)
        {
            _unitOfWork = courseRegisterUnitOfWork;
            _homeworkRepository = _unitOfWork.GetRepository<Homework>();
        }
        public bool AddHomework(HomeworkDto homeworkDto)
        {
            var homework = DtoToEntityMapper.HomeworkDtoToEntity(homeworkDto);     
            _homeworkRepository.Add(homework);
            return _unitOfWork.SaveChanges();
        }
        public int MaxResult(CourseDto courseDto)
        {
            List<HomeworkDto> homeworkList = EntityToDtoMapper.HomeworkEntityToDto(_homeworkRepository.GetAll()
                .Where(a => a.CourseId == courseDto.CourseId)
                .ToList());
            int sumaMaxResult = 0;
            int y = 0;

            foreach (var homework in homeworkList)
            {                          
                y = homework.MaxResult;
                sumaMaxResult = sumaMaxResult + y;
            }
            return sumaMaxResult;
        }

        public int SumResult(CourseDto courseDto, CourseStudentDto courseStudentDto)
        {
            List<HomeworkDto> homeworkList = EntityToDtoMapper.HomeworkEntityToDto(_homeworkRepository.GetAll()
                .Where(a => a.CourseId == courseDto.CourseId)
                .ToList());
            int sumaResult = 0, x;
            foreach (var homework in homeworkList)
            {
                foreach (var result in homework.HomeworkResultList)
                {
                    if (result.Pesel == courseStudentDto.Pesel)
                    {
                        x = result.Result;
                        sumaResult = sumaResult + x;
                    }
                }
            }
            return sumaResult;
        }
    }
   
}

