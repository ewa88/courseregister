﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;
using CourseRegister.DataLayer.Services;
using Ninject.Infrastructure.Language;

namespace CourseRegister.BusinessLayer.Service
{
    public class CourseServices : ICourseServices
    {
        private GenericRepository<Course> _coursesRepository;
        private GenericRepository<CourseStudent> _courseStudentRepository;
        private GenericRepository<Absence> _absenceRepository;
        private GenericRepository<HomeworkResult> _homeworkResultRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;
        private ICourseRepositoryService _courseRepositoryService;
        private IHomeworkRepositoryService _homeworkRepositoryService;

        public CourseServices(ICourseRegisterUnitOfWork courseRegisterUnitOfWork, ICourseRepositoryService courseRepositoryService, IHomeworkRepositoryService homeworkRepositoryService)
        {
            _unitOfWork = courseRegisterUnitOfWork;
            _coursesRepository = _unitOfWork.GetRepository<Course>();
            _courseStudentRepository = _unitOfWork.GetRepository<CourseStudent>();
            _absenceRepository = _unitOfWork.GetRepository<Absence>();
            _homeworkResultRepository = _unitOfWork.GetRepository<HomeworkResult>();
            _courseRepositoryService = courseRepositoryService;
            _homeworkRepositoryService = homeworkRepositoryService;
        }
        public List<CourseStudentDto> GetStudentsByCourseId(int courseId)
        {
            var studentsList = new List<CourseStudentDto>();
            List<CourseDto> courseList = GetCourseById(courseId);
            if (courseList == null || courseList.Count == 0)
            {
                studentsList = null;
            }
            else
            {
                foreach (var course in courseList)
                {
                    studentsList = course.CourseStudentList;
                }
            }
            return studentsList;
        }
        public List<CourseDto> GetCourseById(int courseId)
        {
            List<CourseDto> courseByIdList = EntityToDtoMapper.CourseEntityToDtoModel(_coursesRepository.GetAll()
                .Where(a => a.CourseId == courseId)
                .ToList());
            return courseByIdList;
        }
        public CourseDto GetCourseDtoById(int courseId)
        {
            var course = GetCourseById(courseId).Single();
            return course;
        }
        public bool CheckIfCourseExist(int courseId)
        {
            bool check;
            List<CourseDto> courseList = GetCourseById(courseId);
            if (courseList == null || courseList.Count == 0)
            {
                check = false;
            }
            else
            {
                check = true;
            }
            return check;
        }
        public bool AddCourse(CourseDto courseDto)
        {
            var course = DtoToEntityMapper.CourseDtoToEntityModel(courseDto);
            _coursesRepository.Add(course);
            return _unitOfWork.SaveChanges();
        }
        public bool CheckIfAnyCourseExist()
        {
            bool check;
            List<Course> courseList = _coursesRepository.GetAll().ToList();
            if (courseList.Count == 0)
            {
                check = false;
            }
            else
            {
                check = true;
            }
            return check;
        }
        public List<CourseDto> GetAllCourses()
        {
            return EntityToDtoMapper.CourseEntityToDtoModel(_coursesRepository.GetAll().ToList());
        }
        public bool UpdateCourse(CourseDto course)
        {
            _coursesRepository.UpdateWithNewContext(DtoToEntityMapper.CourseDtoToEntityModel(course));
            return _unitOfWork.SaveChanges();
        }
        public bool CourseStudentRemove(int studentRemoveId, int courseId, long pesel)
        {
            List<Absence> absenceList = _absenceRepository.GetAll()
                .Where(a => a.CourseStudentId == studentRemoveId)
                .ToList();
            List<Homework> homeworkList = _homeworkRepositoryService.GetHomeworkWithHomeworkResultList()
                .Where(a => a.CourseId == courseId)
                .ToList();
          
            var courseStudent = _courseStudentRepository.GetAll()
                .First(a => a.Id == studentRemoveId);
            foreach (var homework in homeworkList)
            {
                foreach (var homeworkResult in homework.HomeworkResultList)
                {
                    if(homeworkResult.Pesel==pesel)
                   _homeworkResultRepository.Delete(homeworkResult);               
                }
            }
            foreach (var absence in absenceList)
            {
                _absenceRepository.Delete(absence);
            }
            _courseStudentRepository.Delete(courseStudent);

            return _unitOfWork.SaveChanges();
        }
        public long GetCourseStudentPeselByCourseStudentId(int id)
        {
            var courseStudent = _courseStudentRepository.GetAll().Single(a => a.Id == id);
            return courseStudent.Pesel;
        }
        public bool CheckIfCourseStudentExist(long pesel)
        {
            bool exist;
            var courseStudentList = _courseStudentRepository.GetAll()
                .Where(a => a.Pesel == pesel)
                .ToList();
            if (courseStudentList.Count != 0)
            {
                exist = false;
            }
            else
            {
                exist = true;
            }
            return exist;
        }
        public bool CheckIfCourseStudentExistById(int id)
        {
            bool exist;
            var courseStudentList = _courseStudentRepository.GetAll().Where(a => a.Id == id).ToList();

            if (courseStudentList.Count != 0)
            {
                exist = true;
            }
            else
            {
                exist = false;
            }
            return exist;
        }
        public bool CheckIfCourseHasNoStudents(int courseId)
        { 
            var courseList = _courseRepositoryService.GetCourseWithCourseStudent()
                .Where(a => a.CourseId == courseId)
                .ToList();
            bool exist=true;
            foreach (var course in courseList)
            {
                if (course.CourseStudentList.Count == 0)
                {
                    exist = false;
                }
            }
            return exist;
        }
    }
}
    
 

