﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;
using CourseRegister.DataLayer.Services;

namespace CourseRegister.BusinessLayer.Service
{
    public class AbsenceService : IAbsenceService
    {
        private GenericRepository<Absence> _absenceRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;

        public AbsenceService(ICourseRegisterUnitOfWork courseRegisterUnitOfWork)
        {
            _unitOfWork = courseRegisterUnitOfWork;
            _absenceRepository = _unitOfWork.GetRepository<Absence>();
        }

        public bool AddAbsenceToStudentsList(AbsenceDto absenceDto)
        {         
            _absenceRepository.Add(DtoToEntityMapper.AbsenceDtoToEntity(absenceDto));
            return _unitOfWork.SaveChanges();           
        }
        public int GetPresenceDaysNumber(CourseStudentDto courseStudentDto)
        {
            int i = 0;
            List<AbsenceDto> courseStudentAbsenceList = EntityToDtoMapper.AbsenceEntityToDto(_absenceRepository.GetAll()
                .Where(a => a.CourseStudentId == courseStudentDto.Id)
                .ToList());
            foreach (var log in courseStudentAbsenceList)
            {

                if (log.Presence == (AbsenceDto.TypeAbsence)Enum.Parse(typeof(AbsenceDto.TypeAbsence), ("o")))
                {
                    i++;
                }
            }
            return i;
        }
        public int GetDaysNumber(CourseStudentDto courseStudentDto)
        {
            List<AbsenceDto> courseStudentAbsenceList = EntityToDtoMapper.AbsenceEntityToDto(_absenceRepository.GetAll()
                .Where(a => a.CourseStudentId == courseStudentDto.Id)
                .ToList());
            return courseStudentAbsenceList.Count;
        }
    }


}
