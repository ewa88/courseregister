﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.BusinessLayer.Mappers;
using CourseRegister.DataLayer.Models;
using CourseRegister.DataLayer.Repositories;
using CourseRegister.DataLayer.Repositories.Interfaces;

namespace CourseRegister.BusinessLayer.Service
{
    public class StudentService : IStudentService
    {
        private GenericRepository<Student> _studentsRepository;
        private ICourseRegisterUnitOfWork _unitOfWork;

        public StudentService(ICourseRegisterUnitOfWork courseRegisterUnitOfWork)
        {
            _unitOfWork = courseRegisterUnitOfWork;
            _studentsRepository = _unitOfWork.GetRepository<Student>();
        }

        public List<Student> GetStudentListByPesel(long pesel)
        {
           return _studentsRepository.GetAll().Where(a => a.PESEL == pesel).ToList();
        }
        public StudentDto GetStudentByPesel(long pesel)
        {
            List<Student> studentsList = GetStudentListByPesel(pesel);
            return EntityToDtoMapper.StudentEntityToDtoModel(studentsList.First());
        }
        public bool CheckIfStudentExistByPesel(long pesel)
        {
            bool exist;
            List<Student> studentsList = GetStudentListByPesel(pesel);

            if (studentsList.Count == 0)
            {
                exist = false;
            }
            else
            {
                exist = true;
            }

            return exist ;
        }
        public bool AddStudent(StudentDto student)
        {
            _studentsRepository.Add(DtoToEntityMapper.StudentDtoToEntityModel(student));
            return _unitOfWork.SaveChanges();
        }
        public List<Student> GetStudentsListById(int id)
        {
            return _studentsRepository.GetAll().Where(a => a.Id == id).ToList();
        }
        public bool CheckIfStudentExistById(int id)
        {
            bool exist;
            List<Student> studentsList = GetStudentsListById(id);

            if (studentsList == null || studentsList.Count == 0)
            {
                exist = false;
            }
            else
            {
                exist = true;
            }

            return exist;
        }
        public StudentDto GetStudentById(int id)
        {
            List<Student> studentsList = GetStudentsListById(id);
            return EntityToDtoMapper.StudentEntityToDtoModel(studentsList.First());
        }
        public List<StudentDto> GetAllStudents()
        {
            return EntityToDtoMapper.StudentsEntityToDtoList(_studentsRepository.GetAll().ToList());;
        }

        public bool UpdateStudent(StudentDto student)
        {
            _studentsRepository.UpdateWithNewContext(DtoToEntityMapper.StudentDtoToEntityModel(student));
            return _unitOfWork.SaveChanges();
        }     
    }
}
