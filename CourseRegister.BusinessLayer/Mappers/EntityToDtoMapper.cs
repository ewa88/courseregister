﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.BusinessLayer.Mappers
{
    public class EntityToDtoMapper
    {
        public static List<StudentDto> StudentsEntityToDtoList(List<Student> studentsList)
        {
            List<StudentDto> studentsDtoList = new List<StudentDto>();
            foreach (var student in studentsList)
            {
                var studentDto = new StudentDto();
                studentDto.Id = student.Id;
                studentDto.Name = student.Name;
                studentDto.Surname = student.Surname;
                studentDto.BirthDate = student.BirthDate;
                studentsDtoList.Add(studentDto);
            }
            return studentsDtoList;
        }

        
        public static List<CourseStudentDto> CourseStudentEntityToDto(List<CourseStudent> courseStudentList)
        {
            List<CourseStudentDto> courseStudentsDtoList = new List<CourseStudentDto>();

            foreach (var courseStudent in courseStudentList)
            {
                CourseStudentDto courseStudentDto = new CourseStudentDto();
                courseStudentDto.Pesel = courseStudent.Pesel;
                courseStudentDto.Id = courseStudent.Id;
                courseStudentsDtoList.Add(courseStudentDto);
            }

            return courseStudentsDtoList;
        }
        public static List<CourseStudentDto> CourseStudentEntityToDtoWithoutAbsenceList(List<CourseStudent> courseStudentList)
        {
            List<CourseStudentDto> courseStudentsDtoList = new List<CourseStudentDto>();

            foreach (var courseStudent in courseStudentList)
            {
                CourseStudentDto courseStudentDto = new CourseStudentDto();
                courseStudentDto.Pesel = courseStudent.Pesel;
                courseStudentDto.Id = courseStudent.Id;

                courseStudentsDtoList.Add(courseStudentDto);
            }

            return courseStudentsDtoList;
        }
        public static List<AbsenceDto> AbsenceEntityToDto(List<Absence> absenceList)
        {
            List<AbsenceDto> absenceDtoList = new List<AbsenceDto>();

            foreach (var absence in absenceList)
            {
                AbsenceDto absenceDto = new AbsenceDto();
                absenceDto.Id = absence.Id;
                absenceDto.Date = absence.Date;
                absenceDto.Presence = (AbsenceDto.TypeAbsence) Enum.Parse(typeof(AbsenceDto.TypeAbsence),
                    (absence.Presence.ToString().ToLower()));
                absenceDtoList.Add(absenceDto);
            }

            return absenceDtoList;
        }

        public static List <CourseDto> CourseEntityToDtoModel(List <Course> courseList)
        {
            List< CourseDto> courseDtoList = new List<CourseDto>();
            foreach (var course in courseList)
            {
                CourseDto courseDto = new CourseDto();
                courseDto.Id = course.Id;
                courseDto.CourseId = course.CourseId;
                courseDto.Name = course.Name;
                courseDto.Date = course.Date;
                courseDto.HomeworkThreshold = course.HomeworkThreshold;
                courseDto.NumberOfStudents = course.NumberOfStudents;
                courseDto.PresenceThreshold = course.PresenceThreshold;
                courseDto.TrainerName = course.TrainerName;
                courseDto.CourseStudentList = CourseStudentEntityToDto(course.CourseStudentList);
              //  courseDto.HomeworkList = HomeworkEntityToDto(course.HomeworkList);
                courseDtoList.Add(courseDto);
            }
            //  Console.WriteLine("mapper po ma studentow:" + course.CourseStudentList.Count);

            return courseDtoList;
        }

        public static StudentDto StudentEntityToDtoModel(Student student)
        {
            StudentDto studentDto = new StudentDto();
            studentDto.Id = student.Id;
            studentDto.Name = student.Name;
            studentDto.Surname = student.Surname;
            studentDto.BirthDate = student.BirthDate;
            studentDto.PESEL = student.PESEL;
            studentDto.Sex = (StudentDto.TypeSex)Enum.Parse(typeof(StudentDto.TypeSex), (student.Sex.ToString().ToUpper()));//zamieniamy na stringa i ze stringa na enuma


            return studentDto;
        }

        public static CourseDto CourseEntityToDto(Course course)
        {
            CourseDto courseDto = new CourseDto();
            courseDto.CourseId = course.CourseId;
            courseDto.HomeworkThreshold = course.HomeworkThreshold;
            courseDto.PresenceThreshold = course.PresenceThreshold;
            courseDto.TrainerName = course.TrainerName;
            courseDto.Name = course.Name;
            courseDto.Id = course.Id;
            courseDto.Date = course.Date;
            courseDto.NumberOfStudents = course.NumberOfStudents;
            courseDto.CourseStudentList = CourseStudentEntityToDto(course.CourseStudentList);
            //courseDto.HomeworkList = HomeworkEntityToDto(course.HomeworkList);
            return courseDto;
        }
        public static CourseStudentDto CourseStudentEntityToDto(CourseStudent courseStudent)
        {
            CourseStudentDto courseStudentDto = new CourseStudentDto();
            courseStudentDto.Id = courseStudent.Id;
            courseStudentDto.Pesel = courseStudent.Pesel;
           
            return courseStudentDto;
        }

        public static List <HomeworkDto> HomeworkEntityToDto(List <Homework> homeworkList)
        {
            List<HomeworkDto> homeworkDtoList=new List<HomeworkDto>();
            foreach (var homework in homeworkList)
            {
                HomeworkDto homeworkDto = new HomeworkDto();
                homeworkDto.CourseId = homework.CourseId;
                homeworkDto.Id = homework.Id;
                homeworkDto.MaxResult = homework.MaxResult;
                homeworkDto.HomeworkResultList = HomeworkResultEntityToDto(homework.HomeworkResultList);
                homeworkDtoList.Add(homeworkDto);
            }
            return homeworkDtoList;
        }

        public static List<HomeworkResultDto> HomeworkResultEntityToDto(List<HomeworkResult> homeworkResultList)
        {
            List<HomeworkResultDto> homeworkResultDtoList = new List<HomeworkResultDto>();
            foreach (var homeworkResult in homeworkResultList)
            {
                HomeworkResultDto homeworkResultDto = new HomeworkResultDto();
                homeworkResultDto.Id = homeworkResult.Id;
                homeworkResultDto.Pesel = homeworkResult.Pesel;
                homeworkResultDto.Result = homeworkResult.Result;
                
                homeworkResultDtoList.Add(homeworkResultDto);
                           
            }
            return homeworkResultDtoList;
        }
    }
}
