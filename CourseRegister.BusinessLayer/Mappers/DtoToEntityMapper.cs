﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.BusinessLayer.Mappers
{
    public class DtoToEntityMapper
    {
        public static Student StudentDtoToEntityModel(StudentDto studentDto)
        {
            Student student = new Student();

            student.Id = studentDto.Id;
            student.Name = studentDto.Name;
            student.Surname = studentDto.Surname;
            student.PESEL = studentDto.PESEL;
            student.BirthDate = studentDto.BirthDate;
            student.Sex = (Student.TypeSex)Enum.Parse(typeof(Student.TypeSex), (studentDto.Sex.ToString().ToUpper()));//zamieniamy na stringa i ze stringa na enuma

            return student;
        }

        public static Course CourseDtoToEntityModel(CourseDto courseDto)
        {
            Course course = new Course();

            course.Id = courseDto.Id;
            course.CourseId = courseDto.CourseId;
            course.Name = courseDto.Name;
            course.Date = courseDto.Date;
            course.HomeworkThreshold = courseDto.HomeworkThreshold;          
            course.NumberOfStudents = courseDto.NumberOfStudents;
            course.PresenceThreshold = courseDto.PresenceThreshold;
            course.TrainerName = courseDto.TrainerName;
            course.CourseStudentList = CourseStudentListDtoToEntity(courseDto.CourseStudentList);
          //  course.HomeworkList = HomeworkDtoToEntityList(courseDto.HomeworkList);
            


            return course;
        }

        public static List <CourseStudent> CourseStudentListDtoToEntity(List<CourseStudentDto> courseStudentDtoList)
        {
          List <CourseStudent> courseStudentsList = new List<CourseStudent>();

            foreach (var courseStudentDto in courseStudentDtoList)
            {
                CourseStudent courseStudent = new CourseStudent();
                courseStudent.Id = courseStudentDto.Id;
                courseStudent.Pesel = courseStudentDto.Pesel;
                courseStudentsList.Add(courseStudent);
            }

            return courseStudentsList;
        }
 
        public static CourseStudent CourseStudentDtoToEntity(CourseStudentDto courseStudentDto)
        {


                CourseStudent courseStudent = new CourseStudent();
                courseStudent.Id = courseStudentDto.Id;
                courseStudent.Pesel = courseStudentDto.Pesel;


            return courseStudent;
        }

        public static List<Absence> AbsenceListDtoToEntity(List <AbsenceDto> absenceDtoList)
        {
            List <Absence> absenceList = new List <Absence>();

            foreach (var absenceDto in absenceDtoList)
            {
                Absence absence = new Absence();
                absence.Id = absenceDto.Id;
                absence.Date = absenceDto.Date;
                absence.Presence = (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), (absenceDto.Presence.ToString().ToLower()));
                absenceList.Add(absence);
            }

            return absenceList;

        }

        public static Absence AbsenceDtoToEntity(AbsenceDto absenceDto)
        {
            Absence absence = new Absence();
            absence.Id = absenceDto.Id;
            absence.CourseStudentId = absenceDto.CourseStudentId;
            absence.Date = absenceDto.Date;
            absence.Presence = (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), (absenceDto.Presence.ToString().ToLower()));


            return absence;
        }

        public static Homework HomeworkDtoToEntity(HomeworkDto homeworkDto)
        {
            Homework homework = new Homework();
            homework.CourseId = homeworkDto.CourseId;
            homework.Id = homeworkDto.Id;
            homework.MaxResult = homeworkDto.MaxResult;
            


            homework.HomeworkResultList = new List<HomeworkResult>();
            foreach (var result in homeworkDto.HomeworkResultList)
            {
                HomeworkResult homeworkResult = new HomeworkResult();
                homeworkResult.Pesel = result.Pesel;
                homeworkResult.Result = result.Result;
                homework.HomeworkResultList.Add(homeworkResult);
            }

            return homework;
        }

        public static List<Homework> HomeworkDtoToEntityList(List<HomeworkDto> homeworkDtoList)
        {
            List<Homework> homeworkList = new List<Homework>();

            if (homeworkDtoList != null)
            {

                foreach (var homeworkDto in homeworkDtoList)
                {
                    Homework homework = new Homework();
                    homework.Id = homeworkDto.Id;
                    homework.MaxResult = homeworkDto.MaxResult;
                    homework.HomeworkResultList = HomeworkResultDtoToEntityList(homeworkDto.HomeworkResultList);
                    homeworkList.Add(homework);
                }
            }
            else
            {
                homeworkList = null;
            }

            return homeworkList;
        }
        public static List <HomeworkResult> HomeworkResultDtoToEntityList(List<HomeworkResultDto> homeworResultDtoList)
        {
            List <HomeworkResult>  homeworkResultsList = new List<HomeworkResult>();
            foreach (var result in homeworResultDtoList)
            {
                HomeworkResult homeworkResult = new HomeworkResult();
                homeworkResult.Id = result.Id;
                homeworkResult.Pesel = result.Pesel;
                homeworkResult.Result = result.Result;
               homeworkResultsList.Add(homeworkResult);
            }
            return homeworkResultsList;
        }

    }


 }

