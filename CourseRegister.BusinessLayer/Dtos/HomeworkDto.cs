﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRegister.BusinessLayer.Dtos
{
    public class HomeworkDto
    {
        public int MaxResult;
        public int Id;
        public int CourseId;

        public List<HomeworkResultDto> HomeworkResultList;

    }
}
