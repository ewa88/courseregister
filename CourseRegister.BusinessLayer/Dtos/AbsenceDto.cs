﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRegister.BusinessLayer.Dtos
{
   public class AbsenceDto
    {
        public enum TypeAbsence
        {
            o,
            n
        }

        public DateTime Date;
        public TypeAbsence Presence;
        public int Id;
        public int CourseStudentId;
    }
}

