﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRegister.BusinessLayer.Dtos
{
    public class CourseDto
    {
        public int Id;
        public int CourseId;
        public string Name;
        public string TrainerName;
        public DateTime Date;
        public int HomeworkThreshold;
        public int PresenceThreshold;
        public int NumberOfStudents;

        public List<CourseStudentDto> CourseStudentList;
       // public List<HomeworkDto> HomeworkList;
    }
}
