﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRegister.BusinessLayer.Dtos
{
    public class StudentDto
    {
        public enum TypeSex
        {
            K,
            M
        }
        public long PESEL;
        public string Name;
        public string Surname;
        public DateTime BirthDate;
        public TypeSex Sex;
        public int Id;

    }
}
