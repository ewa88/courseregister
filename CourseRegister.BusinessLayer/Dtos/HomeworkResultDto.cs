﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRegister.BusinessLayer.Dtos
{
   public class HomeworkResultDto
    {
        public int Result;
        public long Pesel;
        public int Id;
    }
}
