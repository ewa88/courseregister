﻿using System;
using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;

namespace CourseRegister.BusinessLayer.Interfaces
{
    public interface IStudentService
    {
        bool CheckIfStudentExistByPesel(long pesel);
        bool CheckIfStudentExistById(int id);
        bool AddStudent(StudentDto student);
        StudentDto GetStudentByPesel(long pesel);
       // List <CourseStudentDto> GetCourseStudentByPesel(long pesel);
        StudentDto GetStudentById(int id);
        List<StudentDto> GetAllStudents();
       // bool ChangeNameSurname(int studentId, string newName, string newSurname);
        //bool ChangeBirthDate(int studentId, DateTime birthDate);
        bool UpdateStudent(StudentDto student);
    }
}