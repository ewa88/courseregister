﻿using System.Collections.Generic;
using CourseRegister.BusinessLayer.Dtos;
using CourseRegister.DataLayer.Models;

namespace CourseRegister.BusinessLayer.Interfaces
{
    public interface ICourseServices
    {
        List<CourseStudentDto> GetStudentsByCourseId(int courseId);
        bool CheckIfCourseExist(int courseId);
        bool AddCourse(CourseDto course);
        bool CheckIfAnyCourseExist();
        List <CourseDto> GetCourseById(int courseId);
        List<CourseDto> GetAllCourses();
        bool CourseStudentRemove(int studentRemoveId, int courseId, long pesel);
        CourseDto GetCourseDtoById(int courseId);
        bool UpdateCourse(CourseDto courseDto);
        bool CheckIfCourseStudentExist(long pesel);
        bool CheckIfCourseStudentExistById(int id);
        bool CheckIfCourseHasNoStudents(int courseId);
        long GetCourseStudentPeselByCourseStudentId(int id);
    }
}