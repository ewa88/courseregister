﻿using CourseRegister.BusinessLayer.Dtos;

namespace CourseRegister.BusinessLayer.Interfaces
{
    public interface IAbsenceService
    {
        bool AddAbsenceToStudentsList(AbsenceDto absenceDto);
        int GetPresenceDaysNumber(CourseStudentDto courseStudentDto);
        int GetDaysNumber(CourseStudentDto courseStudentDto);
    }
}