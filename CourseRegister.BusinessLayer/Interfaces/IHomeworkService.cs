﻿using CourseRegister.BusinessLayer.Dtos;

namespace CourseRegister.BusinessLayer.Interfaces
{
    public interface IHomeworkService
    {
        int MaxResult(CourseDto courseDto);
        int SumResult(CourseDto courseDto, CourseStudentDto courseStudentDto);
        bool AddHomework(HomeworkDto homeworkDto);
    }
}