﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseRegister.BusinessLayer.Interfaces;
using CourseRegister.BusinessLayer.Service;
using Ninject.Modules;

namespace CourseRegister.BusinessLayer.NinjectModules
{
  public class ServiceModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IStudentService>().To<StudentService>();
            Bind<ICourseServices>().To<CourseServices>();
            Bind<IHomeworkService>().To<HomeworkService>();
            Bind<IAbsenceService>().To<AbsenceService>();
        }
    }
}
